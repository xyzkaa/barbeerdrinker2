(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _welcome_welcome_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./welcome/welcome.component */ "./src/app/welcome/welcome.component.ts");
/* harmony import */ var _bar_details_bar_details_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./bar-details/bar-details.component */ "./src/app/bar-details/bar-details.component.ts");
/* harmony import */ var _beer_details_beer_details_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./beer-details/beer-details.component */ "./src/app/beer-details/beer-details.component.ts");
/* harmony import */ var _beers_beers_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./beers/beers.component */ "./src/app/beers/beers.component.ts");
/* harmony import */ var _drinker_details_drinker_details_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./drinker-details/drinker-details.component */ "./src/app/drinker-details/drinker-details.component.ts");
/* harmony import */ var _drinkers_drinkers_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./drinkers/drinkers.component */ "./src/app/drinkers/drinkers.component.ts");
/* harmony import */ var _manufacturers_manufacturers_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./manufacturers/manufacturers.component */ "./src/app/manufacturers/manufacturers.component.ts");
/* harmony import */ var _insight_insight_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./insight/insight.component */ "./src/app/insight/insight.component.ts");
/* harmony import */ var _bartenders_bartenders_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./bartenders/bartenders.component */ "./src/app/bartenders/bartenders.component.ts");
/* harmony import */ var _modification_modification_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./modification/modification.component */ "./src/app/modification/modification.component.ts");
/* harmony import */ var _sqlqueryinterface_sqlqueryinterface_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./sqlqueryinterface/sqlqueryinterface.component */ "./src/app/sqlqueryinterface/sqlqueryinterface.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'bars'
    },
    {
        path: 'static',
        pathMatch: 'full',
        redirectTo: 'bars'
    },
    {
        path: 'bars',
        pathMatch: 'full',
        component: _welcome_welcome_component__WEBPACK_IMPORTED_MODULE_2__["WelcomeComponent"]
    },
    {
        path: 'bars/:bar',
        pathMatch: 'full',
        component: _bar_details_bar_details_component__WEBPACK_IMPORTED_MODULE_3__["BarDetailsComponent"]
    },
    {
        path: 'beers',
        pathMatch: 'full',
        component: _beers_beers_component__WEBPACK_IMPORTED_MODULE_5__["BeersComponent"]
    },
    {
        path: 'beers/:beer',
        pathMatch: 'full',
        component: _beer_details_beer_details_component__WEBPACK_IMPORTED_MODULE_4__["BeerDetailsComponent"]
    },
    {
        path: 'drinkers',
        pathMatch: 'full',
        component: _drinkers_drinkers_component__WEBPACK_IMPORTED_MODULE_7__["DrinkersComponent"]
    },
    {
        path: 'drinkers/:drinker',
        pathMatch: 'full',
        component: _drinker_details_drinker_details_component__WEBPACK_IMPORTED_MODULE_6__["DrinkerDetailsComponent"]
    },
    {
        path: 'insight',
        pathMatch: 'full',
        component: _insight_insight_component__WEBPACK_IMPORTED_MODULE_9__["InsightComponent"]
    },
    {
        path: 'bartenders',
        pathMatch: 'full',
        component: _bartenders_bartenders_component__WEBPACK_IMPORTED_MODULE_10__["BartendersComponent"]
    },
    {
        path: 'modification',
        pathMatch: 'full',
        component: _modification_modification_component__WEBPACK_IMPORTED_MODULE_11__["ModificationComponent"]
    },
    {
        path: 'sql_query_interface',
        pathMatch: 'full',
        component: _sqlqueryinterface_sqlqueryinterface_component__WEBPACK_IMPORTED_MODULE_12__["SqlqueryinterfaceComponent"]
    },
    {
        path: 'manufacturers',
        pathMatch: 'full',
        component: _manufacturers_manufacturers_component__WEBPACK_IMPORTED_MODULE_8__["ManufacturersComponent"]
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\r\n<nav class=\"navbar navbar-expand-lg navbar-light bg-light\">\r\n  <a class=\"navbar-brand\" routerLink=\"/bars\">Bar Beer Drinker</a>\r\n  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n    <span class=\"navbar-toggler-icon\"></span>\r\n  </button>\r\n\r\n  <div class=\"collapse navbar-collapse\" id=\"navbarNav\">\r\n    <ul class=\"navbar-nav\">\r\n      <li class=\"nav-item\" routerLinkActive=\"active\">\r\n        <a class=\"nav-link\" routerLink=\"/bars\">Bars</a>\r\n      </li>\r\n      <li class=\"nav-item\" routerLinkActive=\"active\">\r\n        <a class=\"nav-link\" routerLink=\"/beers\">Beers</a>\r\n      </li>\r\n      <li class=\"nav-item\" routerLinkActive=\"active\">\r\n        <a class=\"nav-link\" routerLink=\"/drinkers\">Drinkers</a>\r\n      </li>\r\n      <li class=\"nav-item\" routerLinkActive=\"active\">\r\n        <a class=\"nav-link\" routerLink=\"/bartenders\">Bartenders</a>\r\n      </li>\r\n      <li class=\"nav-item\" routerLinkActive=\"active\">\r\n        <a class=\"nav-link\" routerLink=\"/manufacturers\">Manufacturers</a>\r\n      </li>\r\n      <li class=\"nav-item\" routerLinkActive=\"active\">\r\n        <a class=\"nav-link\" routerLink=\"/modification\">Modification</a>\r\n      </li>\r\n      <li class=\"nav-item\" routerLinkActive=\"active\">\r\n              <a class=\"nav-link\" routerLink=\"/sql_query_interface\">SqlQueryInterface</a>\r\n      </li>\r\n\r\n    </ul>\r\n  </div>\r\n</nav>\r\n\r\n<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'bar-beer-drinker-ui';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! primeng/table */ "./node_modules/primeng/table.js");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(primeng_table__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/dropdown */ "./node_modules/primeng/dropdown.js");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(primeng_dropdown__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _welcome_welcome_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./welcome/welcome.component */ "./src/app/welcome/welcome.component.ts");
/* harmony import */ var _bar_details_bar_details_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./bar-details/bar-details.component */ "./src/app/bar-details/bar-details.component.ts");
/* harmony import */ var _beer_details_beer_details_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./beer-details/beer-details.component */ "./src/app/beer-details/beer-details.component.ts");
/* harmony import */ var _beers_beers_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./beers/beers.component */ "./src/app/beers/beers.component.ts");
/* harmony import */ var _insight_insight_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./insight/insight.component */ "./src/app/insight/insight.component.ts");
/* harmony import */ var _drinkers_drinkers_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./drinkers/drinkers.component */ "./src/app/drinkers/drinkers.component.ts");
/* harmony import */ var _drinker_details_drinker_details_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./drinker-details/drinker-details.component */ "./src/app/drinker-details/drinker-details.component.ts");
/* harmony import */ var _bartenders_bartenders_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./bartenders/bartenders.component */ "./src/app/bartenders/bartenders.component.ts");
/* harmony import */ var _modification_modification_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./modification/modification.component */ "./src/app/modification/modification.component.ts");
/* harmony import */ var _sqlqueryinterface_sqlqueryinterface_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./sqlqueryinterface/sqlqueryinterface.component */ "./src/app/sqlqueryinterface/sqlqueryinterface.component.ts");
/* harmony import */ var _manufacturers_manufacturers_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./manufacturers/manufacturers.component */ "./src/app/manufacturers/manufacturers.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"],
                _welcome_welcome_component__WEBPACK_IMPORTED_MODULE_9__["WelcomeComponent"],
                _bar_details_bar_details_component__WEBPACK_IMPORTED_MODULE_10__["BarDetailsComponent"],
                _beer_details_beer_details_component__WEBPACK_IMPORTED_MODULE_11__["BeerDetailsComponent"],
                _beers_beers_component__WEBPACK_IMPORTED_MODULE_12__["BeersComponent"],
                _insight_insight_component__WEBPACK_IMPORTED_MODULE_13__["InsightComponent"],
                _drinkers_drinkers_component__WEBPACK_IMPORTED_MODULE_14__["DrinkersComponent"],
                _drinker_details_drinker_details_component__WEBPACK_IMPORTED_MODULE_15__["DrinkerDetailsComponent"],
                _bartenders_bartenders_component__WEBPACK_IMPORTED_MODULE_16__["BartendersComponent"],
                _modification_modification_component__WEBPACK_IMPORTED_MODULE_17__["ModificationComponent"],
                _sqlqueryinterface_sqlqueryinterface_component__WEBPACK_IMPORTED_MODULE_18__["SqlqueryinterfaceComponent"],
                _manufacturers_manufacturers_component__WEBPACK_IMPORTED_MODULE_19__["ManufacturersComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__["BrowserAnimationsModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_7__["AppRoutingModule"],
                primeng_table__WEBPACK_IMPORTED_MODULE_5__["TableModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                primeng_dropdown__WEBPACK_IMPORTED_MODULE_6__["DropdownModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"]
            ],
            providers: [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/bar-details/bar-details.component.css":
/*!*******************************************************!*\
  !*** ./src/app/bar-details/bar-details.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Jhci1kZXRhaWxzL2Jhci1kZXRhaWxzLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/bar-details/bar-details.component.html":
/*!********************************************************!*\
  !*** ./src/app/bar-details/bar-details.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron jumbotron-fluid\">\r\n  <div class=\"container\">\r\n    <h1 class=\"display-4\">{{ barName }}</h1>\r\n    <p class=\"bar-details\" *ngIf=\"barDetails\">\r\n      {{ barDetails?.Address }} <span *ngIf=\"barDetails.Address && barDetails.City\"> | </span> {{ barDetails?.City}} | {{ barDetails?.State }}\r\n      <br>\r\n      {{ barDetails?.Phone }}\r\n    </p>\r\n    \r\n  </div>\r\n</div>\r\n\r\n<div class=\"container\">\r\n  <h2 class=\"text-center font-weight-light\">Beer Menu</h2>\r\n  <br>\r\n  <div>\r\n    <p-dropdown class=\"float-right text-left\" *ngIf=\"filterOptions\" [options]=\"filterOptions\" (onChange)=\"sortBy($event.value)\"></p-dropdown>\r\n  </div>\r\n  <br>\r\n  <p-table [value]=\"menu\" sortMode=\"multiple\" sortField=\"price\">\r\n  <ng-template pTemplate=\"header\">\r\n  <tr>\r\n  <th>Beer</th>\r\n  <th>Menufacturer</th>\r\n  <th>Price</th>\r\n  <th>Likes</th>\r\n  </tr>\r\n  </ng-template>\r\n  <ng-template pTemplate=\"body\" let-item>\r\n  <tr>\r\n  <td><a routerLink=\"/beers/{{ item.Beer }}\">{{ item.Beer }}</a></td>\r\n  <td>{{ item.Manf }}</td>\r\n  <td>{{ item.Price | currency }}</td>\r\n  <td>{{ item.Likes }}</td>\r\n  </tr>\r\n  </ng-template>\r\n  </p-table>\r\n </div>\r\n<br><br>\r\n\r\n<div class=\"container\">\r\n  <h2 class=\"text-center font-weight-light\" id=\"toptendrinkers\">Top Ten Drinkers by Spending</h2>\r\n  <br>\r\n  \r\n  <div class=\"container\">\r\n    <br>\r\n    <div id=\"bargraph_drinkers\"></div>\r\n  </div>\r\n</div>\r\n\r\n<h2 class=\"text-center font-weight-light\">Fraction of Inventory Sold Each Day</h2>\r\n<br>\r\n\r\n<div class=\"container\">\r\n  <br>\r\n  <div id=\"bargraph_fractions\"></div>\r\n</div>\r\n\r\n<div class=\"container\">\r\n  <h2 class=\"text-center font-weight-light\">Time Distribution of Sales</h2>\r\n  <br>\r\n  \r\n  <div class=\"container\">\r\n    <br>\r\n    <div id=\"bargraph_timedistribution\"></div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"container\">\r\n  <h2 class=\"text-center font-weight-light\" id=\"toptenbeers\">Top Ten Beer Brands Sold</h2>\r\n  <br>\r\n  <div>\r\n    <table>\r\n      <tr><td>Select a Day of the Week:&nbsp;&nbsp;&nbsp; </td>\r\n    <p-dropdown *ngIf=\"dayOptions\" [ngClass]=\"{ 'no-option-selected': selectedDayOfWeek === undefined || selectedDayOfWeek === null }\"\r\n  placeholder=\"Select Day of Week\" [options]=\"dayOptions\" [(ngModel)]=\"selectedDayOfWeek\" showClear=\"true\"\r\n  (onChange)=\"refreshTop10Beer($event.value)\"></p-dropdown>\r\n      </tr></table>\r\n  </div>\r\n  <div class=\"container\">\r\n    <br>\r\n    <div id=\"bargraph_beers\"></div>\r\n  </div>\r\n</div>\r\n\r\n\r\n<br><br>\r\n"

/***/ }),

/***/ "./src/app/bar-details/bar-details.component.ts":
/*!******************************************************!*\
  !*** ./src/app/bar-details/bar-details.component.ts ***!
  \******************************************************/
/*! exports provided: BarDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BarDetailsComponent", function() { return BarDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _bars_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../bars.service */ "./src/app/bars.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BarDetailsComponent = /** @class */ (function () {
    function BarDetailsComponent(barService, route) {
        var _this = this;
        this.barService = barService;
        this.route = route;
        route.paramMap.subscribe(function (paramMap) {
            _this.barName = paramMap.get('bar');
            barService.getBar(_this.barName).subscribe(function (data) {
                _this.barDetails = data;
            }, function (error) {
                if (error.status === 404) {
                    alert('Bar not found');
                }
                else {
                    console.error(error.status + ' - ' + error.body);
                    alert('An error occured on the server. Please check the browser console.');
                }
            });
            barService.getMenu(_this.barName).subscribe(function (data) {
                _this.menu = data;
            }, function (error) {
                if (error.status === 404) {
                    alert('Bar not found');
                }
                else {
                    console.error(error.status + ' - ' + error.body);
                    alert('An error occurred on the server. Please check the browser console.');
                }
            });
            _this.barService.getBarTop10Drinkers(_this.barName).subscribe(function (data) {
                console.log(data);
                var drinkers = [];
                var spents = [];
                data.forEach(function (drinker) {
                    drinkers.push(drinker.Drinker);
                    spents.push(drinker.TotalSpending);
                });
                _this.renderDrinkerChart(drinkers, spents);
            });
            _this.barService.getBarSalesTimeDistribution(_this.barName).subscribe(function (data) {
                console.log(data);
                var intervals = [];
                var sales = [];
                data.forEach(function (interval) {
                    intervals.push(interval.Intervals);
                    sales.push(interval.TotalSales);
                });
                _this.renderTimeDistributionChart(intervals, sales);
            });
            barService.getDaysOfWeek().subscribe(function (data) {
                _this.dayOptions = data.map(function (Day) {
                    return {
                        label: Day,
                        value: Day,
                    };
                });
            });
            _this.barService.getFractionByWeekDay(_this.barName).subscribe(function (data) {
                var fractions = [];
                data.forEach(function (f) {
                    fractions.push(f);
                });
                _this.weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday',
                    'Saturday', 'Sunday'];
                _this.renderFractionsChart(_this.weekdays, fractions);
            });
            _this.filterOptions = [
                {
                    'label': 'Low price first',
                    'value': 'low price'
                },
                {
                    'label': 'High price first',
                    'value': 'high price'
                }
            ];
        });
    }
    BarDetailsComponent.prototype.sortBy = function (selectedOption) {
        if (selectedOption === 'low price') {
            this.menu.sort(function (a, b) {
                return a.Price - b.Price;
            });
        }
        else if (selectedOption === 'high price') {
            this.menu.sort(function (a, b) {
                return b.Price - a.Price;
            });
        }
    };
    BarDetailsComponent.prototype.ngOnInit = function () {
    };
    BarDetailsComponent.prototype.renderChart = function (beers, counts) {
        Highcharts.chart('bargraph_beers', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: beers,
                title: {
                    text: 'Beer'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Number of beers sold'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: [{
                    data: counts
                }]
        });
    };
    BarDetailsComponent.prototype.renderDrinkerChart = function (drinkers, spents) {
        Highcharts.chart('bargraph_drinkers', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: drinkers,
                title: {
                    text: 'Drinker'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Amount Spent ($)'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: [{
                    data: spents
                }]
        });
    };
    BarDetailsComponent.prototype.renderFractionsChart = function (weekdays, fractions) {
        Highcharts.chart('bargraph_fractions', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: weekdays,
                title: {
                    text: 'Weekday'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Fraction of inventory sold'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: [{
                    data: fractions
                }]
        });
    };
    BarDetailsComponent.prototype.renderTimeDistributionChart = function (intervals, sales) {
        Highcharts.chart('bargraph_timedistribution', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: intervals,
                title: {
                    text: 'Day/Time Interval'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total Sales ($)'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: [{
                    data: sales
                }]
        });
    };
    BarDetailsComponent.prototype.refreshTop10Beer = function (Day) {
        var _this = this;
        this.barService.getBarTop10BeersByDay(this.barName, Day).subscribe(function (data) {
            console.log(data);
            var beers = [];
            var counts = [];
            data.forEach(function (beer) {
                beers.push(beer.Beer);
                counts.push(beer.Counts);
            });
            _this.renderChart(beers, counts);
        });
    };
    BarDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-bar-details',
            template: __webpack_require__(/*! ./bar-details.component.html */ "./src/app/bar-details/bar-details.component.html"),
            styles: [__webpack_require__(/*! ./bar-details.component.css */ "./src/app/bar-details/bar-details.component.css")]
        }),
        __metadata("design:paramtypes", [_bars_service__WEBPACK_IMPORTED_MODULE_2__["BarsService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], BarDetailsComponent);
    return BarDetailsComponent;
}());



/***/ }),

/***/ "./src/app/bars.service.ts":
/*!*********************************!*\
  !*** ./src/app/bars.service.ts ***!
  \*********************************/
/*! exports provided: BarsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BarsService", function() { return BarsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BarsService = /** @class */ (function () {
    function BarsService(http) {
        this.http = http;
    }
    BarsService.prototype.getBars = function () {
        return this.http.get('/api/bar');
    };
    BarsService.prototype.getBar = function (bar) {
        return this.http.get('/api/bar/' + bar);
    };
    BarsService.prototype.getMenu = function (bar) {
        return this.http.get('/api/menu/' + bar);
    };
    BarsService.prototype.getFrequentCounts = function () {
        return this.http.get('/api/frequents-data');
    };
    BarsService.prototype.getDaysOfWeek = function () {
        return this.http.get('/api/days-of-week');
    };
    BarsService.prototype.getBarTop10Beers = function (bar) {
        return this.http.get('/api/bar-top10beers/' + bar);
    };
    BarsService.prototype.getBarTop10BeersByDay = function (bar, dw) {
        return this.http.get('/api/bar-top10beers/' + bar + '/' + dw);
    };
    BarsService.prototype.getBarTop10Drinkers = function (bar) {
        return this.http.get('/api/bar-top10drinkers/' + bar);
    };
    BarsService.prototype.getBarSalesTimeDistribution = function (bar) {
        return this.http.get('/api/bar-sales-timedistribution/' + bar);
    };
    BarsService.prototype.getTop10BarsByBeerDay = function (beer, dw) {
        return this.http.get('/api/top10BarsByBeerDay/' + beer + '/' + dw);
    };
    BarsService.prototype.getBeers = function () {
        return this.http.get('/api/beer');
    };
    BarsService.prototype.getBeerNames = function () {
        return this.http.get('/api/beer_names');
    };
    BarsService.prototype.getFractionByWeekDay = function (bar) {
        return this.http.get('/api/inventoryFracByWeekDay/' + bar);
    };
    BarsService.prototype.getTop10BarsBySales = function (beer, inputDay) {
        return this.http.get('/api/top10BarsBySales/' + beer + '/' + inputDay);
    };
    BarsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], BarsService);
    return BarsService;
}());



/***/ }),

/***/ "./src/app/bartenders.service.ts":
/*!***************************************!*\
  !*** ./src/app/bartenders.service.ts ***!
  \***************************************/
/*! exports provided: BartendersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BartendersService", function() { return BartendersService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BartendersService = /** @class */ (function () {
    function BartendersService(http) {
        this.http = http;
    }
    BartendersService.prototype.getBarNames = function () {
        return this.http.get('/api/bar_names');
    };
    BartendersService.prototype.getBartenderNames = function (bar) {
        return this.http.get('/api/bartender_names/' + bar);
    };
    BartendersService.prototype.getBartenderShifts = function (bartender) {
        return this.http.get('/api/bartender_shifts/' + bartender);
    };
    BartendersService.prototype.getBeersSoldByBartender = function (bartender) {
        return this.http.get('/api/beers_sold_by_bartender/' + bartender);
    };
    BartendersService.prototype.getShiftsForBarDay = function (bar, day) {
        return this.http.get('/api/shifts_for_bar_day/' + bar + '/' + day);
    };
    BartendersService.prototype.getBartenderRank = function (bar, day, shift_start) {
        return this.http.get('/api/bartender_rank/' + bar + '/' + day + '/' + shift_start);
    };
    BartendersService.prototype.getDaysOfWeek = function () {
        return this.http.get('/api/days-of-week');
    };
    BartendersService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], BartendersService);
    return BartendersService;
}());



/***/ }),

/***/ "./src/app/bartenders/bartenders.component.css":
/*!*****************************************************!*\
  !*** ./src/app/bartenders/bartenders.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JhcnRlbmRlcnMvYmFydGVuZGVycy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/bartenders/bartenders.component.html":
/*!******************************************************!*\
  !*** ./src/app/bartenders/bartenders.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron jumbotron-fluid\">\r\n  <div class=\"container\">\r\n    <h1 class=\"display-4\">Meet the Bartenders!</h1>\r\n    <p class=\"lead\"></p>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"container\">\r\n  \r\n  <h2 class=\"text-left font-weight-light\">Find a bartender</h2>\r\n  <table>\r\n    <tr><td>Select a Bar:</td>\r\n    <td><p-dropdown *ngIf=\"barOptions1\" [options]=\"barOptions1\" placeholder=\"Select a Bar\" showClear=\"true\"\r\n      (onChange)=\"displayBartenders($event.value)\"></p-dropdown></td> </tr>\r\n    <tr><td>Select a Bartender: &nbsp;&nbsp;&nbsp;</td>\r\n    <td><p-dropdown *ngIf=\"bartenderOptions\" [options]=\"bartenderOptions\" placeholder=\"Select a Bartender\" showClear=\"true\"\r\n      (onChange)=\"showBartenderShiftAndSales($event.value)\"> </p-dropdown></td></tr>    \r\n  </table>\r\n  <br>\r\n    \r\n    <div class=\"container\" id=\"tbl1\">\r\n    \r\n    <h4 class=\"text-center font-weight-light\">{{selectedBartender1}} Work Shifts</h4>\r\n    <p-table [value]=\"bartenderShifts\">\r\n        <ng-template pTemplate=\"header\">\r\n            <tr>\r\n              <th>Bar</th>          \r\n              <th>Day</th>\r\n              <th>Shift Start</th>\r\n              <th>Shift End</th>              \r\n            </tr>\r\n        </ng-template>\r\n        <ng-template pTemplate=\"body\" let-shift>\r\n            <tr>              \r\n              <td>{{shift.Bar}}</td>\r\n              <td>{{shift.Day}}</td>\r\n              <td>{{shift.Shift_Start}}</td>\r\n              <td>{{shift.Shift_End}}</td>\r\n            </tr>\r\n        </ng-template>\r\n      </p-table>\r\n      <br><br>\r\n    </div>\r\n    <div class=\"container\">\r\n                         \r\n          <div class=\"container\">\r\n              <br>\r\n            <div id=\"bargraph_bertendersales\"></div>\r\n          </div>\r\n</div>\r\n\r\n<div class=\"container\">\r\n    <h2 class=\"text-left font-weight-light\">Find the top ten bartenders</h2>  \r\n    <p class=\"lead\"></p>\r\n    <table>\r\n      <tr><td>Select a Bar:</td>\r\n      <td><p-dropdown *ngIf=\"barOptions2\" [options]=\"barOptions2\" placeholder=\"Select a Bar\" [(ngModel)]=\"selectedBar2\" showClear=\"true\"\r\n        (onChange)=\"saveBarOption($event.value)\"></p-dropdown></td> </tr>\r\n      <tr><td>Select a Day:</td>\r\n      <td><p-dropdown *ngIf=\"dayOptions\" [options]=\"dayOptions\" placeholder=\"Select a Day\" [(ngModel)]=\"selectedDay\" showClear=\"true\"\r\n        (onChange)=\"saveDayOption($event.value)\"></p-dropdown></td> </tr>      \r\n      <tr><td>Select a Shift: &nbsp;&nbsp;&nbsp;</td>\r\n      <td><p-dropdown *ngIf=\"shiftOptions\" [options]=\"shiftOptions\" placeholder=\"Select a Shift\" showClear=\"true\"\r\n        (onChange)=\"showTopBartenders($event.value,$event.label)\"> </p-dropdown></td></tr>            \r\n    </table>\r\n    <br><br>     \r\n\r\n   <div class=\"container\">\r\n      <br>\r\n      <div id=\"bargraph_topbartenders\"></div>\r\n      </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/bartenders/bartenders.component.ts":
/*!****************************************************!*\
  !*** ./src/app/bartenders/bartenders.component.ts ***!
  \****************************************************/
/*! exports provided: BartendersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BartendersComponent", function() { return BartendersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _bartenders_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../bartenders.service */ "./src/app/bartenders.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BartendersComponent = /** @class */ (function () {
    function BartendersComponent(bartendersService, route) {
        var _this = this;
        this.bartendersService = bartendersService;
        this.route = route;
        this.display_style = 'None';
        this.bartendersService.getBarNames().subscribe(function (data) {
            _this.barOptions1 = data.map(function (Bar) {
                return {
                    label: Bar,
                    value: Bar,
                };
            });
        });
        this.bartendersService.getBarNames().subscribe(function (data) {
            _this.barOptions2 = data.map(function (Bar) {
                return {
                    label: Bar,
                    value: Bar,
                };
            });
        });
        this.bartendersService.getDaysOfWeek().subscribe(function (data) {
            _this.dayOptions = data.map(function (Day) {
                return {
                    label: Day,
                    value: Day,
                };
            });
        });
    }
    BartendersComponent.prototype.ngOnInit = function () {
    };
    BartendersComponent.prototype.displayBartenders = function (Bar) {
        var _this = this;
        this.selectedBar1 = Bar;
        this.bartendersService.getBartenderNames(Bar).subscribe(function (data) {
            _this.bartenderOptions = data.map(function (Bartender) {
                return {
                    label: Bartender,
                    value: Bartender,
                };
            });
        });
    };
    BartendersComponent.prototype.saveBarOption = function (thisBar) {
        this.selectedBar2 = thisBar;
        if (this.selectedDay) {
            this.getShifts();
        }
    };
    BartendersComponent.prototype.saveDayOption = function (thisDay) {
        this.selectedDay = thisDay;
        if (this.selectedBar2) {
            this.getShifts();
        }
    };
    BartendersComponent.prototype.getShifts = function () {
        var _this = this;
        this.display_style = 'block';
        this.bartendersService.getShiftsForBarDay(this.selectedBar2, this.selectedDay).subscribe(function (data) {
            console.log(data);
            _this.shiftOptions = data.map(function (shifts) {
                return {
                    label: shifts.Shift,
                    value: shifts.Shift_Start
                };
            });
        });
    };
    BartendersComponent.prototype.showTopBartenders = function (shift) {
        var _this = this;
        // this.selectedShift = shift;
        this.selectedShift = shift;
        this.bartendersService.getBartenderRank(this.selectedBar2, this.selectedDay, shift).subscribe(function (data) {
            console.log(data);
            var bartenders = [];
            var sold = [];
            data.forEach(function (bartender) {
                bartenders.push(bartender.Bartender);
                sold.push(bartender.Beer_Sold);
            });
            _this.renderTopBartenderChart(bartenders, sold);
        });
    };
    BartendersComponent.prototype.renderTopBartenderChart = function (bartenders, sold) {
        Highcharts.chart('bargraph_topbartenders', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Top Bartenders by Sales (in ' + this.selectedBar2 + ' on ' + this.selectedDay + ' during the selected shift)'
            },
            xAxis: {
                categories: bartenders,
                title: {
                    text: 'Bartender'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Number of Beers Sold'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: [{
                    data: sold
                }]
        });
    };
    BartendersComponent.prototype.showBartenderShiftAndSales = function (Bartender) {
        var _this = this;
        this.selectedBartender1 = Bartender;
        this.bartendersService.getBartenderShifts(Bartender).subscribe(function (data) {
            _this.bartenderShifts = data;
        });
        this.bartendersService.getBeersSoldByBartender(Bartender).subscribe(function (data) {
            console.log(data);
            var beers = [];
            var sold = [];
            data.forEach(function (beer) {
                beers.push(beer.Beer);
                sold.push(beer.Number_Sold);
            });
            _this.renderBartenderChart(beers, sold);
        });
    };
    BartendersComponent.prototype.renderBartenderChart = function (beers, sold) {
        Highcharts.chart('bargraph_bertendersales', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Top Beer Brands Sold by ' + this.selectedBartender1
            },
            xAxis: {
                categories: beers,
                title: {
                    text: 'Beer'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Number of Beers Sold'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: [{
                    data: sold
                }]
        });
    };
    BartendersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-bartenders',
            template: __webpack_require__(/*! ./bartenders.component.html */ "./src/app/bartenders/bartenders.component.html"),
            styles: [__webpack_require__(/*! ./bartenders.component.css */ "./src/app/bartenders/bartenders.component.css")]
        }),
        __metadata("design:paramtypes", [_bartenders_service__WEBPACK_IMPORTED_MODULE_2__["BartendersService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], BartendersComponent);
    return BartendersComponent;
}());



/***/ }),

/***/ "./src/app/beer-details/beer-details.component.css":
/*!*********************************************************!*\
  !*** ./src/app/beer-details/beer-details.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JlZXItZGV0YWlscy9iZWVyLWRldGFpbHMuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/beer-details/beer-details.component.html":
/*!**********************************************************!*\
  !*** ./src/app/beer-details/beer-details.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron jumbotron-fluid\">\r\n  <div class=\"container\">\r\n    <h1 class=\"display-4\">{{ beerName }}</h1>\r\n    <p class=\"beer-details\" *ngIf=\"manufacturer\">\r\n      Made by <i>{{ manufacturer }}</i>\r\n    </p>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"container\">\r\n  <br>\r\n  <div>\r\n    <span class=\"font-weight-light result-count\">Sold at {{ beerLocations?.length }}</span>\r\n    <p-dropdown class=\"float-right text-left\" *ngIf=\"filterOptions\" [options]=\"filterOptions\" (onChange)=\"sortBy($event.value)\"></p-dropdown>\r\n  </div>\r\n  <br>\r\n  <p-table [value]=\"beerLocations\" sortMode=\"multiple\" sortField=\"price\">\r\n    <ng-template pTemplate=\"header\">\r\n      <tr>\r\n        <th>Bar</th>\r\n        <th>\r\n          Price\r\n        </th>\r\n        <th>Inventory</th>\r\n      </tr>\r\n    </ng-template>\r\n    <ng-template pTemplate=\"body\" let-bar>\r\n      <tr>\r\n        <td><a routerLink=\"/bars/{{ bar.Bar }}\">{{ bar.Bar }}</a></td>\r\n        <td>{{ bar.Price | currency }}</td>\r\n        <td>{{ bar.Inventory }}</td>\r\n      </tr>\r\n    </ng-template>\r\n  </p-table>\r\n</div>\r\n<br>\r\n<div class=\"container\">\r\n<h2 class=\"text-center font-weight-light\">Top Ten Bars Selling {{beerName}}</h2>\r\n<br>\r\n<div id=\"bargraph_bars\"></div>\r\n</div>\r\n<br>\r\n<div class=\"container\">\r\n  <h2 class=\"text-center font-weight-light\">Time Distribution of {{beerName}} Sales</h2>\r\n  <br>\r\n  \r\n<div class=\"container\">\r\n    <br>\r\n    <div id=\"bargraph_timedistribution\"></div>\r\n  </div>\r\n</div>\r\n\r\n<h2 class=\"text-center font-weight-light\">Top 10 Drinkers of {{beerName}}</h2>\r\n<br>\r\n\r\n<div class=\"container\">\r\n<br>\r\n<div id=\"bargraph_drinkers\"></div>\r\n</div>\r\n\r\n "

/***/ }),

/***/ "./src/app/beer-details/beer-details.component.ts":
/*!********************************************************!*\
  !*** ./src/app/beer-details/beer-details.component.ts ***!
  \********************************************************/
/*! exports provided: BeerDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BeerDetailsComponent", function() { return BeerDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _beers_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../beers.service */ "./src/app/beers.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BeerDetailsComponent = /** @class */ (function () {
    function BeerDetailsComponent(beerService, route) {
        var _this = this;
        this.beerService = beerService;
        this.route = route;
        this.route.paramMap.subscribe(function (paramMap) {
            _this.beerName = paramMap.get('beer');
            _this.beerService.getBarsSelling(_this.beerName).subscribe(function (data) {
                _this.beerLocations = data;
            });
            _this.beerService.getBeerManufacturers(_this.beerName)
                .subscribe(function (data) {
                _this.manufacturer = data;
            });
            _this.filterOptions = [
                {
                    'label': 'Low price first',
                    'value': 'low price'
                },
                {
                    'label': 'High price first',
                    'value': 'high price'
                },
                {
                    'label': 'Most inventory first',
                    'value': 'high inventory'
                },
                {
                    'label': 'Least inventory first',
                    'value': 'low inventory'
                }
            ];
        });
        this.beerService.getBeerSalesTimeDistribution(this.beerName).subscribe(function (data) {
            console.log(data);
            var intervals = [];
            var sales = [];
            data.forEach(function (interval) {
                intervals.push(interval.Intervals);
                sales.push(interval.Number_Sold);
            });
            _this.renderTimeDistributionChart(intervals, sales);
        });
        this.beerService.getTop10BarsByBeer(this.beerName).subscribe(function (data) {
            var names = [];
            var count = [];
            data.forEach(function (d) {
                names.push(d.Bar);
                count.push(d.Count);
            });
            _this.renderChart2(names, count);
        });
        this.beerService.getTop10DrinkersByBeer(this.beerName).subscribe(function (data) {
            var names = [];
            var count = [];
            data.forEach(function (d) {
                names.push(d.Drinker);
                count.push(d.Count);
            });
            _this.renderChart(names, count);
        });
    }
    BeerDetailsComponent.prototype.ngOnInit = function () {
    };
    BeerDetailsComponent.prototype.renderChart = function (names, counts) {
        Highcharts.chart('bargraph_drinkers', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: names,
                title: {
                    text: 'Drinker'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Number of Beers Bought'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: [{
                    data: counts
                }]
        });
    };
    BeerDetailsComponent.prototype.renderTimeDistributionChart = function (intervals, sales) {
        Highcharts.chart('bargraph_timedistribution', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: intervals,
                title: {
                    text: 'Day/Time Interval'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Number of Beers Sold'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: [{
                    data: sales
                }]
        });
    };
    BeerDetailsComponent.prototype.renderChart2 = function (names, counts) {
        Highcharts.chart('bargraph_bars', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: names,
                title: {
                    text: 'Bar'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Number of Beers Sold'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: [{
                    data: counts
                }]
        });
    };
    BeerDetailsComponent.prototype.sortBy = function (selectedOption) {
        if (selectedOption === 'low price') {
            this.beerLocations.sort(function (a, b) {
                return a.Price - b.Price;
            });
        }
        else if (selectedOption === 'high price') {
            this.beerLocations.sort(function (a, b) {
                return b.Price - a.Price;
            });
        }
        else if (selectedOption === 'low inventory') {
            this.beerLocations.sort(function (a, b) {
                return a.Inventory - b.Inventory;
            });
        }
        else if (selectedOption === 'high inventory') {
            this.beerLocations.sort(function (a, b) {
                return b.Inventory - a.Inventory;
            });
        }
    };
    BeerDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-beer-details',
            template: __webpack_require__(/*! ./beer-details.component.html */ "./src/app/beer-details/beer-details.component.html"),
            styles: [__webpack_require__(/*! ./beer-details.component.css */ "./src/app/beer-details/beer-details.component.css")]
        }),
        __metadata("design:paramtypes", [_beers_service__WEBPACK_IMPORTED_MODULE_1__["BeersService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], BeerDetailsComponent);
    return BeerDetailsComponent;
}());



/***/ }),

/***/ "./src/app/beers.service.ts":
/*!**********************************!*\
  !*** ./src/app/beers.service.ts ***!
  \**********************************/
/*! exports provided: BeersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BeersService", function() { return BeersService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BeersService = /** @class */ (function () {
    function BeersService(http) {
        this.http = http;
    }
    BeersService.prototype.getBeers = function () {
        return this.http.get('/api/beer');
    };
    BeersService.prototype.getBarsSelling = function (beer) {
        return this.http.get("/api/bars-selling/" + beer);
    };
    BeersService.prototype.getBeerManufacturers = function (beer) {
        if (beer) {
            return this.http.get("/api/beer-manufacturer/" + beer);
        }
        return this.http.get('/api/beer-manufacturer');
    };
    BeersService.prototype.getBeerSalesTimeDistribution = function (beer) {
        return this.http.get('/api/beer-sales-timedistribution/' + beer);
    };
    BeersService.prototype.getTop10BarsByBeer = function (beer) {
        return this.http.get('/api/top10BarsByBeer/' + beer);
    };
    BeersService.prototype.getTop10DrinkersByBeer = function (beer) {
        return this.http.get('/api/top10DrinkersByBeer/' + beer);
    };
    BeersService.prototype.getTop10CitiesByManfRevenue = function (manf) {
        return this.http.get('/api/top10CitiesByManfRevenue/' + manf);
    };
    BeersService.prototype.getTop10CitiesByManfQuantity = function (manf) {
        return this.http.get('/api/top10CitiesByManfQuantity/' + manf);
    };
    BeersService.prototype.get_manf_topregion_bylikes = function (manf) {
        return this.http.get('/api/manf_topregion_bylikes/' + manf);
    };
    BeersService.prototype.getManufacturers = function () {
        return this.http.get('/api/getManufacturers');
    };
    BeersService.prototype.getBeersName = function () {
        return this.http.get('/api/beer-names');
    };
    BeersService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], BeersService);
    return BeersService;
}());



/***/ }),

/***/ "./src/app/beers/beers.component.css":
/*!*******************************************!*\
  !*** ./src/app/beers/beers.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JlZXJzL2JlZXJzLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/beers/beers.component.html":
/*!********************************************!*\
  !*** ./src/app/beers/beers.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron jumbotron-fluid\">\r\n  <div class=\"container\">\r\n    <h1 class=\"display-4\">Find your favorite beers!</h1>\r\n    <p class=\"lead\">This page can be used to see all the beers in our system or just the ones by your preferred\r\n      manufacturer.</p>\r\n    <p-dropdown *ngIf=\"manufacturerOptions\" [ngClass]=\"{ 'no-option-selected': selectedManufacturer === undefined || selectedManufacturer === null }\"\r\n      placeholder=\"Filter by manufacturer\" [options]=\"manufacturerOptions\" [(ngModel)]=\"selectedManufacturer\" showClear=\"true\"\r\n      (onChange)=\"filterBeers($event.value)\"></p-dropdown>\r\n  </div>\r\n</div>\r\n<br>\r\n\r\n<div class=\"container\">\r\n  <h2 class=\"text-center font-weight-light\">Beers</h2>\r\n  <p-table [value]=\"beers\" [paginator]=\"true\" [rows]=\"10\">\r\n    <ng-template pTemplate=\"header\">\r\n      <tr>\r\n        <th>Name</th>\r\n        <th>Manufacturer</th>\r\n      </tr>\r\n    </ng-template>\r\n    <ng-template pTemplate=\"body\" let-beer>\r\n      <tr>\r\n        <td><a routerLink=\"/beers/{{ beer.Name }}\">{{ beer.Name }}</a></td>\r\n        <td>{{ beer.Manf }}</td>\r\n      </tr>\r\n    </ng-template>\r\n  </p-table>\r\n  <br><br>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/beers/beers.component.ts":
/*!******************************************!*\
  !*** ./src/app/beers/beers.component.ts ***!
  \******************************************/
/*! exports provided: BeersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BeersComponent", function() { return BeersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _beers_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../beers.service */ "./src/app/beers.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BeersComponent = /** @class */ (function () {
    function BeersComponent(beerService) {
        var _this = this;
        this.beerService = beerService;
        this.beerService.getBeers().subscribe(function (data) {
            _this.beers = data;
            _this.originalBeersList = data;
        });
        this.beerService.getBeerManufacturers().subscribe(function (data) {
            _this.manufacturerOptions = data.map(function (Manf) {
                return {
                    label: Manf,
                    value: Manf,
                };
            });
        });
    }
    BeersComponent.prototype.ngOnInit = function () {
    };
    BeersComponent.prototype.filterBeers = function (manufacturer) {
        this.beers = this.originalBeersList;
        if (manufacturer) {
            this.beers = this.originalBeersList.filter(function (beer) { return beer.Manf === manufacturer; });
        }
    };
    BeersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-beers',
            template: __webpack_require__(/*! ./beers.component.html */ "./src/app/beers/beers.component.html"),
            styles: [__webpack_require__(/*! ./beers.component.css */ "./src/app/beers/beers.component.css")]
        }),
        __metadata("design:paramtypes", [_beers_service__WEBPACK_IMPORTED_MODULE_1__["BeersService"]])
    ], BeersComponent);
    return BeersComponent;
}());



/***/ }),

/***/ "./src/app/drinker-details/drinker-details.component.css":
/*!***************************************************************!*\
  !*** ./src/app/drinker-details/drinker-details.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RyaW5rZXItZGV0YWlscy9kcmlua2VyLWRldGFpbHMuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/drinker-details/drinker-details.component.html":
/*!****************************************************************!*\
  !*** ./src/app/drinker-details/drinker-details.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron jumbotron-fluid\">\r\n  <div class=\"container\">\r\n    <h1 class=\"display-4\">{{ drinkerName }}</h1>\r\n    <p class=\"drinker-details\" *ngIf=\"drinkerDetails\">\r\n      {{ drinkerDetails?.Address }} <span *ngIf=\"drinkerDetails.Address && drinkerDetails.City\"> | </span> {{ drinkerDetails?.City}} | {{ drinkerDetails?.State }}\r\n      <br>\r\n      {{ drinkerDetails?.Phone }}\r\n    </p>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"container\">\r\n  <h2 class=\"text-center font-weight-light\">{{drinkerName}} Past Transactions</h2>\r\n  <br>\r\n  <p-table [value]=\"transaction\" [paginator]=\"true\" [rows]=\"10\">\r\n  <ng-template pTemplate=\"header\">\r\n  <tr>\r\n  <th>Bar</th>\r\n  <th>Date</th>\r\n  <th>Time</th>\r\n  <th>Total Amount</th>\r\n  <th>Items Bought</th>\r\n  </tr>\r\n  </ng-template>\r\n  <ng-template pTemplate=\"body\" let-item>\r\n  <tr>\r\n  <td>{{ item.Bar }}</td>\r\n  <td>{{ item.Date | date:'MM/dd/yyyy' }}</td>\r\n  <td>{{ item.Time | date:'MM/dd/yyyy HH:mm:ss'}}</td>\r\n  <td>{{ item.TotalAmount | currency }}</td>\r\n  <td>{{ item.ItemsBought }}</td>\r\n  </tr>\r\n  </ng-template>\r\n  </p-table>\r\n </div>\r\n<br><br>\r\n\r\n<div class=\"container\">\r\n  <h2 class=\"text-center font-weight-light\">Top Ten Beers Ordered by {{drinkerName}}</h2>\r\n  <br>\r\n  \r\n  <div class=\"container\">\r\n    <br>\r\n    <div id=\"bargraph_top10beers\"></div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"container\">\r\n  <h2 class=\"text-center font-weight-light\">Spending by {{drinkerName}} in Different Bars</h2>\r\n  <br>\r\n  <div>\r\n    <p-dropdown *ngIf=\"barOptions\" [ngClass]=\"{ 'no-option-selected': selectedBar === undefined || selectedBar === null }\"\r\n  placeholder=\"Select a Bar\" [options]=\"barOptions\" [(ngModel)]=\"selectedBar\" showClear=\"true\"\r\n  (onChange)=\"refreshDrinkerBar($event.value)\"></p-dropdown>\r\n  </div>\r\n  <div class=\"container\">\r\n    <br>\r\n    <div id=\"bargraph_perbarbyday\"></div>\r\n  </div>\r\n  <div class=\"container\">\r\n    <br>\r\n    <div id=\"bargraph_perbarbyweek\"></div>\r\n  </div>\r\n  <div class=\"container\">\r\n    <br>\r\n    <div id=\"bargraph_perbarbymonth\"></div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/drinker-details/drinker-details.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/drinker-details/drinker-details.component.ts ***!
  \**************************************************************/
/*! exports provided: DrinkerDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DrinkerDetailsComponent", function() { return DrinkerDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _drinkers_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../drinkers.service */ "./src/app/drinkers.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DrinkerDetailsComponent = /** @class */ (function () {
    function DrinkerDetailsComponent(drinkersService, route) {
        var _this = this;
        this.drinkersService = drinkersService;
        this.route = route;
        route.paramMap.subscribe(function (paramMap) {
            _this.drinkerName = paramMap.get('drinker');
            drinkersService.getDrinker(_this.drinkerName).subscribe(function (data) {
                _this.drinkerDetails = data;
            }, function (error) {
                if (error.status === 404) {
                    alert('Drinker not found');
                }
                else {
                    console.error(error.status + ' - ' + error.body);
                    alert('An error occured on the server. Please check the browser console.');
                }
            });
            drinkersService.getDrinkerTransactions(_this.drinkerName).subscribe(function (data) {
                _this.transaction = data;
            }, function (error) {
                if (error.status === 404) {
                    alert('Drinker not found');
                }
                else {
                    console.error(error.status + ' - ' + error.body);
                    alert('An error occurred on the server. Please check the browser console.');
                }
            });
            _this.drinkersService.getDrinkerBars(_this.drinkerName).subscribe(function (data) {
                _this.barOptions = data.map(function (Bar) {
                    return {
                        label: Bar,
                        value: Bar,
                    };
                });
            });
            _this.drinkersService.getDrinkerTop10Beer(_this.drinkerName).subscribe(function (data) {
                console.log(data);
                var beers = [];
                var ordqty = [];
                data.forEach(function (beer) {
                    beers.push(beer.Beer);
                    ordqty.push(beer.OrdQty);
                });
                _this.renderDrinkerTop10BeerChart(beers, ordqty);
            });
        });
    }
    DrinkerDetailsComponent.prototype.ngOnInit = function () {
    };
    DrinkerDetailsComponent.prototype.renderDrinkerTop10BeerChart = function (beers, ordqty) {
        Highcharts.chart('bargraph_top10beers', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: beers,
                title: {
                    text: 'Beer'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Number of beers ordered'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: [{
                    data: ordqty
                }]
        });
    };
    DrinkerDetailsComponent.prototype.refreshDrinkerBar = function (Bar) {
        var _this = this;
        this.drinkersService.getDrinkerBarByDay(this.drinkerName, Bar).subscribe(function (data) {
            console.log(data);
            var weekdays = [];
            var spents_day = [];
            data.forEach(function (weekday) {
                weekdays.push(weekday.WeekDay);
                spents_day.push(weekday.TotalSpending);
            });
            _this.renderDrinkerBarByDayChart(weekdays, spents_day);
        });
        this.drinkersService.getDrinkerBarByWeek(this.drinkerName, Bar).subscribe(function (data) {
            console.log(data);
            var weeks = [];
            var spents_week = [];
            data.forEach(function (week) {
                weeks.push(week.WeekOfYear);
                spents_week.push(week.TotalSpending);
            });
            _this.renderDrinkerBarByWeekChart(weeks, spents_week);
        });
        this.drinkersService.getDrinkerBarByMonth(this.drinkerName, Bar).subscribe(function (data) {
            console.log(data);
            var months = [];
            var spents_month = [];
            data.forEach(function (month) {
                months.push(month.Month);
                spents_month.push(month.TotalSpending);
            });
            _this.renderDrinkerBarByMonthChart(months, spents_month);
        });
    };
    DrinkerDetailsComponent.prototype.renderDrinkerBarByDayChart = function (weekdays, spents) {
        Highcharts.chart('bargraph_perbarbyday', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'By Weekday'
            },
            xAxis: {
                categories: weekdays,
                title: {
                    text: 'Day'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Amount Spent'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: [{
                    data: spents
                }]
        });
    };
    DrinkerDetailsComponent.prototype.renderDrinkerBarByWeekChart = function (weeks, spents) {
        Highcharts.chart('bargraph_perbarbyweek', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'By Week of Year'
            },
            xAxis: {
                categories: weeks,
                title: {
                    text: 'Week of Year'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Amount Spent'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: [{
                    data: spents
                }]
        });
    };
    DrinkerDetailsComponent.prototype.renderDrinkerBarByMonthChart = function (months, spents) {
        Highcharts.chart('bargraph_perbarbymonth', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'By Month'
            },
            xAxis: {
                categories: months,
                title: {
                    text: 'Month'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Amount Spent'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: [{
                    data: spents
                }]
        });
    };
    DrinkerDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-drinker-details',
            template: __webpack_require__(/*! ./drinker-details.component.html */ "./src/app/drinker-details/drinker-details.component.html"),
            styles: [__webpack_require__(/*! ./drinker-details.component.css */ "./src/app/drinker-details/drinker-details.component.css")]
        }),
        __metadata("design:paramtypes", [_drinkers_service__WEBPACK_IMPORTED_MODULE_2__["DrinkersService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], DrinkerDetailsComponent);
    return DrinkerDetailsComponent;
}());



/***/ }),

/***/ "./src/app/drinkers.service.ts":
/*!*************************************!*\
  !*** ./src/app/drinkers.service.ts ***!
  \*************************************/
/*! exports provided: DrinkersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DrinkersService", function() { return DrinkersService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DrinkersService = /** @class */ (function () {
    function DrinkersService(http) {
        this.http = http;
    }
    DrinkersService.prototype.getDrinkers = function () {
        return this.http.get('/api/drinker');
    };
    DrinkersService.prototype.getDrinker = function (drinker) {
        return this.http.get('/api/drinker/' + drinker);
    };
    DrinkersService.prototype.getDrinkerTransactions = function (drinker) {
        return this.http.get('/api/drinker-transactions/' + drinker);
    };
    DrinkersService.prototype.getDrinkerTop10Beer = function (drinker) {
        return this.http.get('/api/drinker-top10beer/' + drinker);
    };
    DrinkersService.prototype.getDrinkerBars = function (drinker) {
        return this.http.get('/api/drinker-bars/' + drinker);
    };
    DrinkersService.prototype.getDrinkerBarByDay = function (drinker, bar) {
        return this.http.get('/api/drinker-bar-byday/' + drinker + '/' + bar);
    };
    DrinkersService.prototype.getDrinkerBarByWeek = function (drinker, bar) {
        return this.http.get('/api/drinker-bar-byweek/' + drinker + '/' + bar);
    };
    DrinkersService.prototype.getDrinkerBarByMonth = function (drinker, bar) {
        return this.http.get('/api/drinker-bar-bymonth/' + drinker + '/' + bar);
    };
    DrinkersService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], DrinkersService);
    return DrinkersService;
}());



/***/ }),

/***/ "./src/app/drinkers/drinkers.component.css":
/*!*************************************************!*\
  !*** ./src/app/drinkers/drinkers.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RyaW5rZXJzL2RyaW5rZXJzLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/drinkers/drinkers.component.html":
/*!**************************************************!*\
  !*** ./src/app/drinkers/drinkers.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron jumbotron-fluid\">\r\n  <div class=\"container\">\r\n    <h1 class=\"display-4\">These are the drinkers!</h1>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"container\">\r\n  <p-table [value]=\"drinkers\" [paginator]=\"true\" [rows]=\"10\">\r\n    <ng-template pTemplate=\"header\">\r\n        <tr>\r\n          <th>Name</th>          \r\n          <th>Address</th>\r\n          <th>City</th>\r\n          <th>State</th>\r\n          <th>Phone</th>\r\n        </tr>\r\n    </ng-template>\r\n    <ng-template pTemplate=\"body\" let-drinker>\r\n        <tr>\r\n          <td>\r\n            <a routerLink=\"/drinkers/{{drinker.Name}}\">\r\n              {{drinker.Name}}\r\n            </a>\r\n          </td>\r\n          <td>{{drinker.Address}}</td>\r\n          <td>{{drinker.City}}</td>\r\n          <td>{{drinker.State}}</td>\r\n          <td>{{drinker.Phone}}</td>\r\n        </tr>\r\n    </ng-template>\r\n  </p-table>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/drinkers/drinkers.component.ts":
/*!************************************************!*\
  !*** ./src/app/drinkers/drinkers.component.ts ***!
  \************************************************/
/*! exports provided: DrinkersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DrinkersComponent", function() { return DrinkersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _drinkers_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../drinkers.service */ "./src/app/drinkers.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DrinkersComponent = /** @class */ (function () {
    function DrinkersComponent(drinkerService) {
        this.drinkerService = drinkerService;
        this.getDrinkers();
    }
    DrinkersComponent.prototype.ngOnInit = function () {
    };
    DrinkersComponent.prototype.getDrinkers = function () {
        var _this = this;
        this.drinkerService.getDrinkers().subscribe(function (data) {
            _this.drinkers = data;
        }, function (error) {
            alert('Could not retrieve a list of drinkers');
        });
    };
    DrinkersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-drinkers',
            template: __webpack_require__(/*! ./drinkers.component.html */ "./src/app/drinkers/drinkers.component.html"),
            styles: [__webpack_require__(/*! ./drinkers.component.css */ "./src/app/drinkers/drinkers.component.css")]
        }),
        __metadata("design:paramtypes", [_drinkers_service__WEBPACK_IMPORTED_MODULE_1__["DrinkersService"]])
    ], DrinkersComponent);
    return DrinkersComponent;
}());



/***/ }),

/***/ "./src/app/insight/insight.component.css":
/*!***********************************************!*\
  !*** ./src/app/insight/insight.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2luc2lnaHQvaW5zaWdodC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/insight/insight.component.html":
/*!************************************************!*\
  !*** ./src/app/insight/insight.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron jumbotron-fluid\">\r\n  <div class=\"container\">\r\n    <h1 class=\"display-4\">Welcome to Bar Beer Drinker!</h1>\r\n    <p class=\"lead\">This is a demo application to learn how to create a clean, responsive web interface.</p>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"container\">\r\n  <br>\r\n  <div id=\"bargraph\"></div>\r\n</div>"

/***/ }),

/***/ "./src/app/insight/insight.component.ts":
/*!**********************************************!*\
  !*** ./src/app/insight/insight.component.ts ***!
  \**********************************************/
/*! exports provided: InsightComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InsightComponent", function() { return InsightComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _bars_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../bars.service */ "./src/app/bars.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InsightComponent = /** @class */ (function () {
    function InsightComponent(barService) {
        var _this = this;
        this.barService = barService;
        this.barService.getFrequentCounts().subscribe(function (data) {
            console.log(data);
            var bars = [];
            var counts = [];
            data.forEach(function (bar) {
                bars.push(bar.Bar);
                counts.push(bar.frequentCount);
            });
            _this.renderChart(bars, counts);
        });
    }
    InsightComponent.prototype.ngOnInit = function () {
    };
    InsightComponent.prototype.renderChart = function (bars, counts) {
        Highcharts.chart('bargraph', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Frequenting count at bars'
            },
            xAxis: {
                categories: bars,
                title: {
                    text: 'Bar'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Number of customers'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: [{
                    data: counts
                }]
        });
    };
    InsightComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-insight',
            template: __webpack_require__(/*! ./insight.component.html */ "./src/app/insight/insight.component.html"),
            styles: [__webpack_require__(/*! ./insight.component.css */ "./src/app/insight/insight.component.css")]
        }),
        __metadata("design:paramtypes", [_bars_service__WEBPACK_IMPORTED_MODULE_1__["BarsService"]])
    ], InsightComponent);
    return InsightComponent;
}());



/***/ }),

/***/ "./src/app/manufacturers/manufacturers.component.css":
/*!***********************************************************!*\
  !*** ./src/app/manufacturers/manufacturers.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21hbnVmYWN0dXJlcnMvbWFudWZhY3R1cmVycy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/manufacturers/manufacturers.component.html":
/*!************************************************************!*\
  !*** ./src/app/manufacturers/manufacturers.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron jumbotron-fluid\">\r\n  <div class=\"container\">\r\n    <h1 class=\"display-4\">Here are the Manufacturers!</h1>\r\n    <p class=\"lead\"></p>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"container\">\r\n\r\n  <h2 class=\"text-left font-weight-light\">Find a Manufacturer</h2>\r\n  <br>\r\n  <table>\r\n    <tr><td>Select a Manufacturer:</td>\r\n    <td><p-dropdown *ngIf=\"manfOptions\" [options]=\"manfOptions\" placeholder=\"Select a Manufacturer\" showClear=\"true\"\r\n      (onChange)=\"showManufacturersData($event.value)\"></p-dropdown></td> </tr>\r\n  </table>\r\n  <br>\r\n  <div class=\"container\">\r\n     <br>\r\n     <div id=\"bargraph_cityRevenue\"></div>\r\n     </div>\r\n    <div class=\"container\">\r\n        <br>\r\n        <div id=\"bargraph_cityQuantity\"></div>\r\n      </div>\r\n\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/manufacturers/manufacturers.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/manufacturers/manufacturers.component.ts ***!
  \**********************************************************/
/*! exports provided: ManufacturersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManufacturersComponent", function() { return ManufacturersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _beers_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../beers.service */ "./src/app/beers.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ManufacturersComponent = /** @class */ (function () {
    function ManufacturersComponent(beerService, route) {
        var _this = this;
        this.beerService = beerService;
        this.route = route;
        this.beerService.getBeerManufacturers().subscribe(function (data) {
            _this.manfOptions = data.map(function (Manf) {
                return {
                    label: Manf,
                    value: Manf,
                };
            });
        });
    }
    ManufacturersComponent.prototype.ngOnInit = function () {
    };
    ManufacturersComponent.prototype.showManufacturersData = function (manf) {
        var _this = this;
        this.manf = manf;
        this.beerService.get_manf_topregion_bylikes(manf).subscribe(function (data) {
            var citystates2 = [];
            var likes = [];
            data.forEach(function (citystate2) {
                citystates2.push(citystate2.CityState);
                likes.push(citystate2.Likes);
            });
            _this.renderManfChartByQuantity(citystates2, likes);
        });
        this.beerService.getTop10CitiesByManfRevenue(manf).subscribe(function (data) {
            console.log(data);
            var citystates = [];
            var revenue = [];
            data.forEach(function (citystate) {
                citystates.push(citystate.CityState);
                revenue.push(citystate.Revenue);
            });
            _this.renderManfChart(citystates, revenue);
        });
    };
    /*beerService.getBeerManufacturers().subscribe(
      data => {
        this.manfOptions = data.map(Manf => {
          return {
            label: Manf,
            value: Manf,
          };
        }
        );
      }

    )*/
    ManufacturersComponent.prototype.renderManfChart = function (cityState, revenue) {
        Highcharts.chart('bargraph_cityRevenue', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Top Ten City/States that Generated the Most Revenue from ' + this.manf + ' Products Last Week'
            },
            xAxis: {
                categories: cityState,
                title: {
                    text: 'City/State'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Revenue ($)'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: [{
                    data: revenue
                }]
        });
    };
    ManufacturersComponent.prototype.renderManfChartByQuantity = function (cityState2, likes) {
        Highcharts.chart('bargraph_cityQuantity', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Top Ten City/States That Like ' + this.manf + ' Beers Most'
            },
            xAxis: {
                categories: cityState2,
                title: {
                    text: 'City/State'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Number of Likes'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: [{
                    data: likes
                }]
        });
    };
    ManufacturersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-manufacturers',
            template: __webpack_require__(/*! ./manufacturers.component.html */ "./src/app/manufacturers/manufacturers.component.html"),
            styles: [__webpack_require__(/*! ./manufacturers.component.css */ "./src/app/manufacturers/manufacturers.component.css")]
        }),
        __metadata("design:paramtypes", [_beers_service__WEBPACK_IMPORTED_MODULE_2__["BeersService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], ManufacturersComponent);
    return ManufacturersComponent;
}());



/***/ }),

/***/ "./src/app/modification.service.ts":
/*!*****************************************!*\
  !*** ./src/app/modification.service.ts ***!
  \*****************************************/
/*! exports provided: ModificationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModificationService", function() { return ModificationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ModificationService = /** @class */ (function () {
    function ModificationService(http) {
        this.http = http;
    }
    ModificationService.prototype.postModification = function (sqlStmt_str) {
        return this.http.post('/api/modification', { 'modSql': sqlStmt_str });
    };
    ModificationService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ModificationService);
    return ModificationService;
}());



/***/ }),

/***/ "./src/app/modification/modification.component.css":
/*!*********************************************************!*\
  !*** ./src/app/modification/modification.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZGlmaWNhdGlvbi9tb2RpZmljYXRpb24uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/modification/modification.component.html":
/*!**********************************************************!*\
  !*** ./src/app/modification/modification.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron jumbotron-fluid\">\r\n  <div class=\"container\">\r\n    <h1 class=\"display-4\">Bar Beer Drinker Admin</h1>\r\n    <p class=\"lead\">This page is for administrators to update/delete/insert records in all tables.</p> \r\n    <p>Note: Constraints and assertions are applied wherever applicable. Cascading delete and update on child tables with foreign keys are applied if the corresponding records in parent tables are deleted or updated.</p>    \r\n  </div>\r\n</div>\r\n\r\n<div class=\"container\">\r\n    <h2 class=\"text-left font-weight-light\">Bars</h2>\r\n    <h6 class=\"text-left font-weight-light\">Name varchar(100), License varchar(15), Phone varchar(15), Address varchar(75), City varchar(50), State varchar(25)</h6>\r\n    <h6 class=\"text-left font-weight-light\">Primary Key (Name)</h6> \r\n  <input #bars_Sql (keyup)=\"onKey($event)\" class=\"text-left font-weight-light\" style='width:80%'>\r\n  <button (click)=\"run_bars_sql(bars_Sql.value)\" >Execute Sql</button>\r\n  <br>\r\n  <p class=\"text-left font-weight-light\">SQL Execution Status: <span style='color:blue'>{{rspBarsMessage}}</span></p>\r\n</div>\r\n\r\n<div class=\"container\">\r\n    <h2 class=\"text-left font-weight-light\">Drinkers</h2>\r\n    <h6 class=\"text-left font-weight-light\">Name varchar(75), City varchar(50), State varchar(25), Address varchar(75), Phone varchar(15)</h6>\r\n    <h6 class=\"text-left font-weight-light\">Primary Key (Name)</h6> \r\n  <input #drinkers_Sql (keyup)=\"onKey($event)\" class=\"text-left font-weight-light\" style='width:80%'>\r\n  <button (click)=\"run_drinkers_sql(drinkers_Sql.value)\" >Execute Sql</button>\r\n  <br>\r\n  <p class=\"text-left font-weight-light\">SQL Execution Status: <span style='color:blue'>{{rspDrinkersMessage}}</span></p>\r\n</div>\r\n\r\n<div class=\"container\">\r\n    <h2 class=\"text-left font-weight-light\">Items</h2>\r\n    <h6 class=\"text-left font-weight-light\">Name varchar(100), Manf varchar(100), Type varchar(10)</h6>\r\n    <h6 class=\"text-left font-weight-light\">Primary Key (Name)</h6> \r\n  <input #items_Sql (keyup)=\"onKey($event)\" class=\"text-left font-weight-light\" style='width:80%'>\r\n  <button (click)=\"run_items_sql(items_Sql.value)\" >Execute Sql</button>\r\n  <br>\r\n  <p class=\"text-left font-weight-light\">SQL Execution Status: <span style='color:blue'>{{rspItemsMessage}}</span></p>\r\n</div>\r\n\r\n<div class=\"container\">\r\n    <h2 class=\"text-left font-weight-light\">Bartenders</h2>\r\n    <h6 class=\"text-left font-weight-light\">Name varchar(75), City varchar(50), State varchar(25)</h6>\r\n    <h6 class=\"text-left font-weight-light\">Primary Key (Name)</h6> \r\n  <input #bartenders_Sql (keyup)=\"onKey($event)\" class=\"text-left font-weight-light\" style='width:80%'>\r\n  <button (click)=\"run_bartenders_sql(bartenders_Sql.value)\" >Execute Sql</button>\r\n  <br>\r\n  <p class=\"text-left font-weight-light\">SQL Execution Status: <span style='color:blue'>{{rspBartendersMessage}}</span></p>\r\n</div>\r\n\r\n<div class=\"container\">\r\n    <h2 class=\"text-left font-weight-light\">Transaction_Bills</h2>\r\n    <h6 class=\"text-left font-weight-light\">TransactionID int, Bar varchar(100), TransactionTime datetime, Drinker varchar(75), Bartender varchar(100), TotalAmount_PostTax decimal(10,2), Tip decimal(10,2)</h6>\r\n    <h6 class=\"text-left font-weight-light\">Primary Key (TransactionID, Bar), Foreign Keys: Bar, Drinker, Bartender</h6> \r\n  <input #transaction_bills_Sql (keyup)=\"onKey($event)\" class=\"text-left font-weight-light\" style='width:80%'>\r\n  <button (click)=\"run_transaction_bills_sql(transaction_bills_Sql.value)\" >Execute Sql</button>\r\n  <br>\r\n  <p class=\"text-left font-weight-light\">SQL Execution Status: <span style='color:blue'>{{rspTransaction_BillsMessage}}</span></p>\r\n</div>\r\n\r\n<div class=\"container\">\r\n    <h2 class=\"text-left font-weight-light\">Bar_Hours</h2>\r\n    <h6 class=\"text-left font-weight-light\">Bar varchar(100), Day varchar(20), Open_hour int, Close_hour int</h6>\r\n    <h6 class=\"text-left font-weight-light\">Foreign Keys: Bar, Day</h6> \r\n  <input #bar_hours_Sql (keyup)=\"onKey($event)\" class=\"text-left font-weight-light\" style='width:80%'>\r\n  <button (click)=\"run_bar_hours_sql(bar_hours_Sql.value)\" >Execute Sql</button>\r\n  <br>\r\n  <p class=\"text-left font-weight-light\">SQL Execution Status: <span style='color:blue'>{{rspBar_HoursMessage}}</span></p>\r\n</div>\r\n\r\n<div class=\"container\">\r\n    <h2 class=\"text-left font-weight-light\">Works</h2>\r\n    <h6 class=\"text-left font-weight-light\">Bartender varchar(100), Bar varchar(100), Day varchar(20), Shift_Start int, Shift_End int</h6>\r\n    <h6 class=\"text-left font-weight-light\">Foreign Keys: Bartender, Bar, Day</h6> \r\n  <input #works_Sql (keyup)=\"onKey($event)\" class=\"text-left font-weight-light\" style='width:80%'>\r\n  <button (click)=\"run_works_sql(works_Sql.value)\" >Execute Sql</button>\r\n  <br>\r\n  <p class=\"text-left font-weight-light\">SQL Execution Status: <span style='color:blue'>{{rspWorksMessage}}</span></p>\r\n</div>\r\n\r\n<div class=\"container\">\r\n    <h2 class=\"text-left font-weight-light\">Sells</h2>\r\n    <h6 class=\"text-left font-weight-light\">Bar varchar(100), Item varchar(100), Price decimal(6,2), Inventory int</h6>\r\n    <h6 class=\"text-left font-weight-light\">Foreign Keys: Bar, Item</h6> \r\n  <input #sells_Sql (keyup)=\"onKey($event)\" class=\"text-left font-weight-light\" style='width:80%'>\r\n  <button (click)=\"run_sells_sql(sells_Sql.value)\" >Execute Sql</button>\r\n  <br>\r\n  <p class=\"text-left font-weight-light\">SQL Execution Status: <span style='color:blue'>{{rspSellsMessage}}</span></p>\r\n</div>\r\n\r\n<div class=\"container\">\r\n    <h2 class=\"text-left font-weight-light\">Buys</h2>\r\n    <h6 class=\"text-left font-weight-light\">TransactionID int, Bar varchar(100), Item varchar(100), Quantity int</h6>\r\n    <h6 class=\"text-left font-weight-light\">Foreign Keys: (TransactionID, Bar), Item</h6> \r\n  <input #buys_Sql (keyup)=\"onKey($event)\" class=\"text-left font-weight-light\" style='width:80%'>\r\n  <button (click)=\"run_buys_sql(buys_Sql.value)\" >Execute Sql</button>\r\n  <br>\r\n  <p class=\"text-left font-weight-light\">SQL Execution Status: <span style='color:blue'>{{rspBuysMessage}}</span></p>\r\n</div>\r\n\r\n<div class=\"container\">\r\n    <h2 class=\"text-left font-weight-light\">Frequents</h2>\r\n    <h6 class=\"text-left font-weight-light\">Drinker varchar(100), Bar varchar(100)</h6>\r\n    <h6 class=\"text-left font-weight-light\">Foreign Keys: Drinker, Bar</h6> \r\n  <input #frequents_Sql (keyup)=\"onKey($event)\" class=\"text-left font-weight-light\" style='width:80%'>\r\n  <button (click)=\"run_frequents_sql(frequents_Sql.value)\" >Execute Sql</button>\r\n  <br>\r\n  <p class=\"text-left font-weight-light\">SQL Execution Status: <span style='color:blue'>{{rspFrequentsMessage}}</span></p>\r\n</div>\r\n\r\n<div class=\"container\">\r\n    <h2 class=\"text-left font-weight-light\">Likes</h2>\r\n    <h6 class=\"text-left font-weight-light\">Drinker varchar(100), Beer varchar(100)</h6>\r\n    <h6 class=\"text-left font-weight-light\">Foreign Keys: Drinker, Beer</h6> \r\n  <input #likes_Sql (keyup)=\"onKey($event)\" class=\"text-left font-weight-light\" style='width:80%'>\r\n  <button (click)=\"run_likes_sql(likes_Sql.value)\" >Execute Sql</button>\r\n  <br>\r\n  <p class=\"text-left font-weight-light\">SQL Execution Status: <span style='color:blue'>{{rspLikesMessage}}</span></p>\r\n</div>"

/***/ }),

/***/ "./src/app/modification/modification.component.ts":
/*!********************************************************!*\
  !*** ./src/app/modification/modification.component.ts ***!
  \********************************************************/
/*! exports provided: ModificationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModificationComponent", function() { return ModificationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _modification_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../modification.service */ "./src/app/modification.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ModificationComponent = /** @class */ (function () {
    function ModificationComponent(modService, route) {
        this.modService = modService;
        this.route = route;
        this.rspBarsMessage = '';
        this.rspDrinkersMessage = '';
        this.rspItemsMessage = '';
        this.rspBartendersMessage = '';
        this.rspTransaction_BillsMessage = '';
        this.rspBar_HoursMessage = '';
        this.rspWorksMessage = '';
        this.rspBuysMessage = '';
        this.rspFrequentsMessage = '';
        this.rspLikesMessage = '';
        this.rspSellsMessage = '';
    }
    ModificationComponent.prototype.ngOnInit = function () {
    };
    ModificationComponent.prototype.onKey = function (event) {
        // this.rspBarsMessage = '';
    };
    ModificationComponent.prototype.run_bars_sql = function (str_sqlStmt) {
        var _this = this;
        this.rspBarsMessage = '';
        this.modService.postModification(str_sqlStmt).subscribe(function (data) {
            console.log(data);
            _this.rspBarsMessage = 'Sql Execution succeeded.';
        }, function (error) {
            _this.rspBarsMessage = 'error = ' + error.error;
            console.error(error.status + ' - ' + error.error);
            /* alert('An error occured on the server. Please check the browser console.'); */
        });
    };
    ModificationComponent.prototype.run_drinkers_sql = function (str_sqlStmt) {
        var _this = this;
        this.rspDrinkersMessage = '';
        this.modService.postModification(str_sqlStmt).subscribe(function (data) {
            console.log(data);
            _this.rspDrinkersMessage = 'Sql Execution succeeded.';
        }, function (error) {
            _this.rspDrinkersMessage = 'error = ' + error.error;
            console.error(error.status + ' - ' + error.error);
            /* alert('An error occured on the server. Please check the browser console.'); */
        });
    };
    ModificationComponent.prototype.run_items_sql = function (str_sqlStmt) {
        var _this = this;
        this.rspItemsMessage = '';
        this.modService.postModification(str_sqlStmt).subscribe(function (data) {
            console.log(data);
            _this.rspItemsMessage = 'Sql Execution succeeded.';
        }, function (error) {
            _this.rspItemsMessage = 'error = ' + error.error;
            console.error(error.status + ' - ' + error.error);
            /* alert('An error occured on the server. Please check the browser console.'); */
        });
    };
    ModificationComponent.prototype.run_bartenders_sql = function (str_sqlStmt) {
        var _this = this;
        this.rspBartendersMessage = '';
        this.modService.postModification(str_sqlStmt).subscribe(function (data) {
            console.log(data);
            _this.rspBartendersMessage = 'Sql Execution succeeded.';
        }, function (error) {
            _this.rspBartendersMessage = 'error = ' + error.error;
            console.error(error.status + ' - ' + error.error);
            /* alert('An error occured on the server. Please check the browser console.'); */
        });
    };
    ModificationComponent.prototype.run_transaction_bills_sql = function (str_sqlStmt) {
        var _this = this;
        this.rspTransaction_BillsMessage = '';
        this.modService.postModification(str_sqlStmt).subscribe(function (data) {
            console.log(data);
            _this.rspTransaction_BillsMessage = 'Sql Execution succeeded.';
        }, function (error) {
            _this.rspTransaction_BillsMessage = 'error = ' + error.error;
            console.error(error.status + ' - ' + error.error);
            /* alert('An error occured on the server. Please check the browser console.'); */
        });
    };
    ModificationComponent.prototype.run_bar_hours_sql = function (str_sqlStmt) {
        var _this = this;
        this.rspBar_HoursMessage = '';
        this.modService.postModification(str_sqlStmt).subscribe(function (data) {
            console.log(data);
            _this.rspBar_HoursMessage = 'Sql Execution succeeded.';
        }, function (error) {
            _this.rspBar_HoursMessage = 'error = ' + error.error;
            console.error(error.status + ' - ' + error.error);
            /* alert('An error occured on the server. Please check the browser console.'); */
        });
    };
    ModificationComponent.prototype.run_works_sql = function (str_sqlStmt) {
        var _this = this;
        this.rspWorksMessage = '';
        this.modService.postModification(str_sqlStmt).subscribe(function (data) {
            console.log(data);
            _this.rspWorksMessage = 'Sql Execution succeeded.';
        }, function (error) {
            _this.rspWorksMessage = 'error = ' + error.error;
            console.error(error.status + ' - ' + error.error);
            /* alert('An error occured on the server. Please check the browser console.'); */
        });
    };
    ModificationComponent.prototype.run_buys_sql = function (str_sqlStmt) {
        var _this = this;
        this.rspBuysMessage = '';
        this.modService.postModification(str_sqlStmt).subscribe(function (data) {
            console.log(data);
            _this.rspBuysMessage = 'Sql Execution succeeded.';
        }, function (error) {
            _this.rspBuysMessage = 'error = ' + error.error;
            console.error(error.status + ' - ' + error.error);
            /* alert('An error occured on the server. Please check the browser console.'); */
        });
    };
    ModificationComponent.prototype.run_frequents_sql = function (str_sqlStmt) {
        var _this = this;
        this.rspFrequentsMessage = '';
        this.modService.postModification(str_sqlStmt).subscribe(function (data) {
            console.log(data);
            _this.rspFrequentsMessage = 'Sql Execution succeeded.';
        }, function (error) {
            _this.rspFrequentsMessage = 'error = ' + error.error;
            console.error(error.status + ' - ' + error.error);
            /* alert('An error occured on the server. Please check the browser console.'); */
        });
    };
    ModificationComponent.prototype.run_likes_sql = function (str_sqlStmt) {
        var _this = this;
        this.rspLikesMessage = '';
        this.modService.postModification(str_sqlStmt).subscribe(function (data) {
            console.log(data);
            _this.rspLikesMessage = 'Sql Execution succeeded.';
        }, function (error) {
            _this.rspLikesMessage = 'error = ' + error.error;
            console.error(error.status + ' - ' + error.error);
            /* alert('An error occured on the server. Please check the browser console.'); */
        });
    };
    ModificationComponent.prototype.run_sells_sql = function (str_sqlStmt) {
        var _this = this;
        this.rspSellsMessage = '';
        this.modService.postModification(str_sqlStmt).subscribe(function (data) {
            console.log(data);
            _this.rspSellsMessage = 'Sql Execution succeeded.';
        }, function (error) {
            _this.rspSellsMessage = 'error = ' + error.error;
            console.error(error.status + ' - ' + error.error);
            /* alert('An error occured on the server. Please check the browser console.'); */
        });
    };
    ModificationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-modification',
            template: __webpack_require__(/*! ./modification.component.html */ "./src/app/modification/modification.component.html"),
            styles: [__webpack_require__(/*! ./modification.component.css */ "./src/app/modification/modification.component.css")]
        }),
        __metadata("design:paramtypes", [_modification_service__WEBPACK_IMPORTED_MODULE_2__["ModificationService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], ModificationComponent);
    return ModificationComponent;
}());



/***/ }),

/***/ "./src/app/sqlqueryinterface.service.ts":
/*!**********************************************!*\
  !*** ./src/app/sqlqueryinterface.service.ts ***!
  \**********************************************/
/*! exports provided: SqlqueryinterfaceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SqlqueryinterfaceService", function() { return SqlqueryinterfaceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SqlqueryinterfaceService = /** @class */ (function () {
    function SqlqueryinterfaceService(http) {
        this.http = http;
    }
    SqlqueryinterfaceService.prototype.postSqlqueryinterface = function (sqlStmt_str) {
        var options = { responseType: 'text' };
        return this.http.post('/api/sql_query_interface', { 'selSql': sqlStmt_str }, options);
    };
    SqlqueryinterfaceService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], SqlqueryinterfaceService);
    return SqlqueryinterfaceService;
}());



/***/ }),

/***/ "./src/app/sqlqueryinterface/sqlqueryinterface.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/sqlqueryinterface/sqlqueryinterface.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NxbHF1ZXJ5aW50ZXJmYWNlL3NxbHF1ZXJ5aW50ZXJmYWNlLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/sqlqueryinterface/sqlqueryinterface.component.html":
/*!********************************************************************!*\
  !*** ./src/app/sqlqueryinterface/sqlqueryinterface.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron jumbotron-fluid\">\r\n  <div class=\"container\">\r\n    <h1 class=\"display-4\">Bar Beer Drinker SQL Query Interface</h1>\r\n    <p class=\"lead\">This page is for SELECT. Type the SQL query in the box and it will be evaluated in the database with results returned. \r\n      <br>To speed up the execution, limit the select to just a few hundred rows.\r\n      <br>For Updates/Deletions/Insertions, please use the Modification page.</p>\r\n  </div>\r\n</div>\r\n  <div class=\"container\">\r\n  <input #newSql (keyup)=\"onKey($event)\" style='width:80%;' >\r\n  <button (click)=\"run_sql(newSql.value)\" >Execute Sql</button>\r\n  <br><br>\r\n  <p>Sql Execution Status: <font color=\"blue\">{{rspMessage}}</font></p>\r\n  <pre>Sql Execution Results:<br>{{str_myData}}</pre>\r\n\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/sqlqueryinterface/sqlqueryinterface.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/sqlqueryinterface/sqlqueryinterface.component.ts ***!
  \******************************************************************/
/*! exports provided: SqlqueryinterfaceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SqlqueryinterfaceComponent", function() { return SqlqueryinterfaceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _sqlqueryinterface_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../sqlqueryinterface.service */ "./src/app/sqlqueryinterface.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SqlqueryinterfaceComponent = /** @class */ (function () {
    function SqlqueryinterfaceComponent(modService, route) {
        this.modService = modService;
        this.route = route;
        this.rspMessage = '';
    }
    SqlqueryinterfaceComponent.prototype.ngOnInit = function () {
    };
    SqlqueryinterfaceComponent.prototype.onKey = function (event) {
        // this.rspMessage = '';
        // this.str_myData = '';
    };
    SqlqueryinterfaceComponent.prototype.run_sql = function (str_sqlStmt) {
        var _this = this;
        this.rspMessage = '';
        this.str_myData = '';
        this.modService.postSqlqueryinterface(str_sqlStmt).subscribe(function (data) {
            _this.str_myData = data;
            console.log(data);
            _this.rspMessage = 'Sql Exection succeeded.';
        }, function (error) {
            _this.rspMessage = 'error = ' + error.error;
            console.error(error.status + ' - ' + error.error);
            /* alert('An error occured on the server. Please check the browser console.'); */
        });
    };
    SqlqueryinterfaceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sqlqueryinterface',
            template: __webpack_require__(/*! ./sqlqueryinterface.component.html */ "./src/app/sqlqueryinterface/sqlqueryinterface.component.html"),
            styles: [__webpack_require__(/*! ./sqlqueryinterface.component.css */ "./src/app/sqlqueryinterface/sqlqueryinterface.component.css")]
        }),
        __metadata("design:paramtypes", [_sqlqueryinterface_service__WEBPACK_IMPORTED_MODULE_2__["SqlqueryinterfaceService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], SqlqueryinterfaceComponent);
    return SqlqueryinterfaceComponent;
}());



/***/ }),

/***/ "./src/app/welcome/welcome.component.css":
/*!***********************************************!*\
  !*** ./src/app/welcome/welcome.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3dlbGNvbWUvd2VsY29tZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/welcome/welcome.component.html":
/*!************************************************!*\
  !*** ./src/app/welcome/welcome.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron jumbotron-fluid\">\r\n  <div class=\"container\">\r\n    <h1 class=\"display-4\">Welcome to Bar Beer Drinker!</h1>\r\n    <p class=\"lead\"></p>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"container\">\r\n  <p-table [value]=\"bars\" [paginator]=\"true\" [rows]=\"10\">\r\n    <ng-template pTemplate=\"header\">\r\n        <tr>\r\n          <th>Name</th>\r\n          <th>License</th>\r\n          <th>Address</th>\r\n          <th>City</th>\r\n          <th>State</th>\r\n          <th>Phone</th>\r\n        </tr>\r\n    </ng-template>\r\n    <ng-template pTemplate=\"body\" let-bar>\r\n        <tr>\r\n          <td>\r\n            <a routerLink=\"/bars/{{bar.Name}}\">\r\n              {{bar.Name}}\r\n            </a>\r\n          </td>\r\n          <td>{{bar.License}}</td>\r\n          <td>{{bar.Address}}</td>\r\n          <td>{{bar.City}}</td>\r\n          <td>{{bar.State}}</td>\r\n          <td>{{bar.Phone}}</td>\r\n        </tr>\r\n    </ng-template>\r\n  </p-table>\r\n</div>\r\n<br><br>\r\n<div class=\"container\">\r\n    <h2 class=\"text-left font-weight-light\">Find the top ten bars selling the specified beer on a given day</h2>  \r\n    <p class=\"lead\"></p>\r\n    <table>\r\n      <tr><td>Select a Beer:</td>  \r\n      <td><p-dropdown *ngIf=\"beerOptions\" [options]=\"beerOptions\" placeholder=\"Select a Beer\" showClear=\"true\"  \r\n        (onChange)=\"saveBeers($event.value)\"></p-dropdown></td> </tr>  \r\n      <tr><td>Select a Day:</td>  \r\n      <td><p-dropdown *ngIf=\"dayOptions\" [options]=\"dayOptions\" placeholder=\"Select a Day\" showClear=\"true\"  \r\n        (onChange)=\"saveDays($event.value)\"></p-dropdown></td> </tr>  \r\n    </table>    \r\n\r\n   <div class=\"container\">\r\n      <br>\r\n      <div id=\"bargraph_top10barsbysales\"></div>\r\n      </div>\r\n</div>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/welcome/welcome.component.ts":
/*!**********************************************!*\
  !*** ./src/app/welcome/welcome.component.ts ***!
  \**********************************************/
/*! exports provided: WelcomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WelcomeComponent", function() { return WelcomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _bars_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../bars.service */ "./src/app/bars.service.ts");
/* harmony import */ var _beers_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../beers.service */ "./src/app/beers.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var WelcomeComponent = /** @class */ (function () {
    function WelcomeComponent(barService, beerService) {
        var _this = this;
        this.barService = barService;
        this.beerService = beerService;
        this.getBars();
        this.beerService.getBeersName().subscribe(function (data) {
            _this.beerOptions = data.map(function (Beer) {
                return {
                    label: Beer,
                    value: Beer,
                };
            });
        });
        this.barService.getDaysOfWeek().subscribe(function (data) {
            _this.dayOptions = data.map(function (Day) {
                return {
                    label: Day,
                    value: Day,
                };
            });
        });
    }
    WelcomeComponent.prototype.ngOnInit = function () {
    };
    WelcomeComponent.prototype.getBars = function () {
        var _this = this;
        this.barService.getBars().subscribe(function (data) {
            _this.bars = data;
        }, function (error) {
            alert('Could not retrieve a list of bars');
        });
    };
    WelcomeComponent.prototype.saveBeers = function (thisBeer) {
        this.selectedBeer = thisBeer;
        if (this.selectedDay) {
            this.getSales();
        }
    };
    WelcomeComponent.prototype.saveDays = function (thisDay) {
        this.selectedDay = thisDay;
        if (this.selectedBeer) {
            this.getSales();
        }
    };
    WelcomeComponent.prototype.getSales = function () {
        var _this = this;
        this.barService.getTop10BarsBySales(this.selectedBeer, this.selectedDay).subscribe(function (data) {
            console.log(data);
            var bars = [];
            var count = [];
            data.forEach(function (d) {
                bars.push(d.Bar);
                count.push(d.Count);
            });
            _this.renderChart(bars, count);
        });
    };
    WelcomeComponent.prototype.renderChart = function (bars, count) {
        Highcharts.chart('bargraph_top10barsbysales', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: bars,
                title: {
                    text: 'Bar'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total Beers Sold'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: [{
                    data: count
                }]
        });
    };
    WelcomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-welcome',
            template: __webpack_require__(/*! ./welcome.component.html */ "./src/app/welcome/welcome.component.html"),
            styles: [__webpack_require__(/*! ./welcome.component.css */ "./src/app/welcome/welcome.component.css")]
        }),
        __metadata("design:paramtypes", [_bars_service__WEBPACK_IMPORTED_MODULE_1__["BarsService"],
            _beers_service__WEBPACK_IMPORTED_MODULE_2__["BeersService"]])
    ], WelcomeComponent);
    return WelcomeComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\KAA\Documents\Rutgers\Fall 2018\336_Databases\barbeerdrinker2\bar_beer_drinker\bar-beer-drinker-ui\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map