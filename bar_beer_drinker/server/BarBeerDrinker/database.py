from sqlalchemy import create_engine
from sqlalchemy import sql
from BarBeerDrinker import config
engine = create_engine(config.database_uri)

def get_bars():
    """
    Connect to the database and retrieve a list of all the bars and their information
    """
    with engine.connect() as con:
        rs = con.execute("SELECT Name, License, City, Phone, Address, State FROM Bars where Name in (select distinct Bar from Transaction_Bills);")
        return [dict(row) for row in rs]

def find_bar(name):    
    with engine.connect() as con:
        # give bar details given a bar name
        query = sql.text(
            "SELECT Name, License, City, Phone, Address, State FROM Bars WHERE Name = :name;"
        )
        rs = con.execute(query, name=name)
        result = rs.first()
        if result is None:
            return None
        return dict(result)

def get_bar_menu(bar_name):    
    with engine.connect() as con:
        # get beer menu for a given bar, and likes for the beer (in all bars)
        query = sql.text(
            "SELECT a.Bar, a.Item as Beer, a.Price, b.Manf, coalesce(c.like_count, 0) as Likes \
                FROM Sells as a \
                JOIN Items as b \
                ON a.Item = b.Name \
                  and b.Type = 'Beer'\
                LEFT OUTER JOIN (SELECT Beer, count(*) as like_count FROM Likes GROUP BY Beer) as c \
                ON a.Item = c.Beer \
                WHERE a.Bar = :bar_name;"
        )
        rs = con.execute(query, bar_name=bar_name)
        results = [dict(row) for row in rs]
        for i, _ in enumerate(results):
            results[i]['Price'] = float(results[i]['Price'])
        return results

def get_bar_top10beers(bar):
    with engine.connect() as con:
        # find top beers sold in a given bar            
        query = sql.text('select b.Item Beer, sum(b.Quantity) Counts \
        from Buys b  join Items i on b.Item = i.Name and i.Type = "Beer" \
        where b.Bar = :bar \
        group by b.Item order by 2 desc Limit 10;')
        # query = sql.text('call sp_get_bar_top10beers(:bar,"All");')
        rs = con.execute(query, bar=bar)
        results = [dict(row) for row in rs]
        if results is None:
                return None
        for r in results:
            r['Counts'] = int(r['Counts'])
        return results        

def get_bar_top10beers_byday(bar, dw):
    with engine.connect() as con: 
        # find top beers sold in a given bar on a given weekday           
        query = sql.text('select b.Item Beer, sum(Quantity) Counts from Buys b \
        join Transaction_Bills t on b.TransactionID = t.TransactionID and b.Bar = t.Bar and b.Bar = :bar \
        join Items i on b.Item = i.Name and i.Type = "Beer" \
        where dayname(case when hour(t.TransactionTime) < 8 then date_add(t.TransactionTime, interval -1 day) else t.TransactionTime end) = :dw \
        group by b.Item order by 2 desc Limit 10;')
        rs = con.execute(query, bar=bar, dw=dw)
        results = [dict(row) for row in rs]
        if results is None:
            return None
        for r in results:
            r['Counts'] = int(r['Counts'])
        return results

def get_bar_top10drinkers(bar):
    with engine.connect() as con:
        # find top spending drinkers in a given bar
        query = sql.text('select Drinker, sum(TotalAmount_PostTax) TotalSpending \
        from Transaction_Bills \
        where Bar = :bar \
        group by Drinker order by 2 desc limit 10; ')
        rs = con.execute(query, bar=bar)
        results = [dict(row) for row in rs]
        if results is None:
                 return None
        for r in results:
            r['TotalSpending'] = float(r['TotalSpending'])
        return results

def get_bar_sales_timedistribution(bar):
    with engine.connect() as con:
        # find sale distribution by each weekday and every two-hour interval in a given bar
        query = sql.text('select b.Intervals, sum(a.TotalAmount_PostTax) TotalSales \
        from Transaction_Bills a join DayTimeInterval b \
        on a.Bar = :bar and binary(dayname(case when hour(a.TransactionTime) < 8 then date_add(a.TransactionTime, interval -1 day) else a.TransactionTime end)) = binary(b.Day) \
        and date_format(a.TransactionTime, "%H:%m:%s") between b.StartTime and b.EndTime \
        group by b.Intervals order by b.Sort; ')
        rs = con.execute(query, bar=bar)
        results = [dict(row) for row in rs]
        if results is None:
                 return None
        for r in results:
            r['TotalSales'] = float(r['TotalSales'])
        return results

def get_beers():
    """Gets a list of beer names from the beers table."""

    with engine.connect() as con:
        rs = con.execute('SELECT Name, Manf FROM Items where Type = "Beer";')
        return [dict(row) for row in rs]


def get_beer_manufacturers(beer):
    with engine.connect() as con:
        # get manufacturer of a specified beer or a full list of manufactrures
        if beer is None:
            rs = con.execute('SELECT DISTINCT Manf FROM Items where Type = "Beer";')
            return [row['Manf'] for row in rs]

        query = sql.text('SELECT Manf FROM Items WHERE Type = "Beer" and Name = :beer;')
        rs = con.execute(query, beer=beer)
        result = rs.first()
        if result is None:
            return None
        return result['Manf']


def filter_beers(max_price):
    with engine.connect() as con:
        query = sql.text(
            "SELECT * FROM Sells WHERE Price < :max_price;"
        )
        rs = con.execute(query, max_price=max_price)
        results = [dict(row) for row in rs]
        for r in results:
            r['Price'] = float(r['Price'])
        return results

def get_bars_selling(beer):
    with engine.connect() as con:
        # find all bars selling a beer brand
        query = sql.text('SELECT a.Bar, a.Price, a.Inventory \
                FROM Sells AS a \
                WHERE a.Item = :beer \
                ORDER BY a.Price; \
            ')
        rs = con.execute(query, beer=beer)
        results = [dict(row) for row in rs]
        for i, _ in enumerate(results):
            results[i]['Price'] = float(results[i]['Price'])
        return results

def get_drinkers():
    with engine.connect() as con:
        # get the list of drinkers
        query = sql.text('SELECT Name, Address, City, State, Phone FROM Drinkers where Name in (select distinct Drinker from Transaction_Bills);')               
        rs = con.execute(query)
        return [dict(row) for row in rs]

def get_drinker_info(drinker):
    with engine.connect() as con:
        # get drinker details of a given drinker
        query = sql.text('SELECT Name, Address, City, State, Phone FROM Drinkers WHERE Name = :name;')
        rs = con.execute(query, name=drinker)
        result = rs.first()
        if result is None:
            return None
        return dict(result)

def get_drinker_transactions(drinker):
    with engine.connect() as con:
        # get past transactions of a drinker, including all items purchased
        query = sql.text('select a.Bar, cast(case when hour(a.TransactionTime) < 8 then date_add(a.TransactionTime, interval -1 day) else a.TransactionTime end as Date) Date, \
                a.TransactionTime Time, a.TotalAmount_PostTax TotalAmount, \
                group_concat(concat(b.Item, " (",b.Quantity, ") ")) ItemsBought \
                from Transaction_Bills a join Buys b on a.Bar = b.Bar and a.TransactionID = b.TransactionID \
                where a.Drinker = :drinker \
                group by a.Bar, a.TransactionTime, a.TotalAmount_PostTax order by 1,2,3;')
        rs = con.execute(query, drinker=drinker)
        results = [dict(row) for row in rs]
        for r in results:
            r['TotalAmount'] = float(r['TotalAmount'])
        return results

def get_drinker_top10beer(drinker):
    with engine.connect() as con:
        # find top ten beers consumed by a drinker
        query = sql.text('select b.Item Beer, sum(b.Quantity) OrdQty \
        from Transaction_Bills a join Buys b on a.Drinker = :drinker and a.TransactionID = b.TransactionID and a.Bar = b.Bar \
        join Items c on b.Item = c.Name and c.Type = "Beer" \
        group by b.Item order by 2 desc limit 10;')
        rs = con.execute(query, drinker=drinker)
        results = [dict(row) for row in rs]
        for r in results:
                r['OrdQty'] = int(r['OrdQty'])
        return results

def get_drinker_bars(drinker):
    with engine.connect() as con:
        # list bars that a drinker had transactions with
        query = sql.text('select distinct Bar from Transaction_Bills where Drinker = :drinker;')
        rs = con.execute(query, drinker=drinker)
        return [row['Bar'] for row in rs]
        
def get_drinker_bar_byday(drinker, bar):
    with engine.connect() as con:
        # find a drinker's spending in a given bar on each weekday/weekend
        query = sql.text('select dayname(case when hour(TransactionTime) < 8 then date_add(TransactionTime, interval -1 day) else TransactionTime end) WeekDay, \
        sum(TotalAmount_PostTax) TotalSpending, \
        dayofweek(case when hour(TransactionTime) < 8 then date_add(TransactionTime, interval -1 day) else TransactionTime end) dw \
        from Transaction_Bills where Drinker = :drinker and Bar = :bar \
        group by dayname(case when hour(TransactionTime) < 8 then date_add(TransactionTime, interval -1 day) else TransactionTime end) \
        order by 3;')
        rs = con.execute(query, drinker=drinker, bar=bar)
        results = [dict(row) for row in rs]
        for r in results:
                r['TotalSpending'] = float(r['TotalSpending'])
        return results        

def get_drinker_bar_byweek(drinker, bar):
    with engine.connect() as con:
        # find a drinker's spending in a given bar in each week of the year
        query = sql.text('select week(case when hour(TransactionTime) < 8 then date_add(TransactionTime, interval -1 day) else TransactionTime end) WeekOfYear, \
        sum(TotalAmount_PostTax) TotalSpending \
        from Transaction_Bills where Drinker = :drinker and Bar = :bar \
        group by week(case when hour(TransactionTime) < 8 then date_add(TransactionTime, interval -1 day) else TransactionTime end) \
        order by 1;')
        rs = con.execute(query, drinker=drinker, bar=bar)
        results = [dict(row) for row in rs]
        for r in results:
                r['TotalSpending'] = float(r['TotalSpending'])
        return results

def get_drinker_bar_bymonth(drinker, bar):
    with engine.connect() as con:
        # find a drinker's spending in a given bar in each month
        query = sql.text('select monthname(case when hour(TransactionTime) < 8 then date_add(TransactionTime, interval -1 day) else TransactionTime end) Month, \
        sum(TotalAmount_PostTax) TotalSpending, \
        month(case when hour(TransactionTime) < 8 then date_add(TransactionTime, interval -1 day) else TransactionTime end) Month1 \
        from Transaction_Bills where Drinker = :drinker and Bar = :bar \
        group by monthname(case when hour(TransactionTime) < 8 then date_add(TransactionTime, interval -1 day) else TransactionTime end) \
        order by 3;')
        rs = con.execute(query, drinker=drinker, bar=bar)
        results = [dict(row) for row in rs]
        for r in results:
                r['TotalSpending'] = float(r['TotalSpending'])
        return results

def get_bar_frequent_counts():
    with engine.connect() as con:
        query = sql.text('SELECT Bar, count(*) as frequentCount \
                FROM Frequents \
                GROUP BY Bar; \
            ')
        rs = con.execute(query)
        results = [dict(row) for row in rs]
        return results

def get_bar_cities():
    with engine.connect() as con:
        rs = con.execute('SELECT DISTINCT City FROM Bars;')
        return [row['City'] for row in rs]

def get_bar_states():
    with engine.connect() as con:
        rs = con.execute('SELECT DISTINCT State FROM Bars;')
        return [row['State'] for row in rs]

def get_days_of_week():
    with engine.connect() as con:
        rs = con.execute('select "Monday" as Day,1 union \
                        select "Tuesday",2 union \
                        select "Wednesday",3 union \
                        select "Thursday",4 union \
                        select "Friday",5 union \
                        select "Saturday",6 union \
                        select "Sunday",7 \
                        order by 2 ;')      
        return [row['Day'] for row in rs]

def get_likes(drinker_name):
    """Gets a list of beers liked by the drinker provided."""

    with engine.connect() as con:
        query = sql.text('SELECT Beer FROM Likes WHERE Drinker = :name;')
        rs = con.execute(query, name=drinker_name)
        return [row['Beer'] for row in rs]

def get_bar_names():
    """Gets a list of all bar names."""

    with engine.connect() as con:
        query=sql.text('SELECT Name as Bar FROM Bars WHERE Name in (SELECT distinct Bar from Transaction_Bills);')
        rs=con.execute(query)
        return [row['Bar'] for row in rs]

def get_bartender_names(bar_name):
    """Gets a list of all bartender names."""

    with engine.connect() as con:
        query=sql.text('SELECT distinct Bartender FROM Works WHERE Bar=:name order by 1;')
        rs=con.execute(query,name=bar_name)
        return [row['Bartender'] for row in rs]

def get_bartender_shifts(bartender_name):
    """Gets a list of shifts for a bartender at a bar."""

    with engine.connect() as con:
        query=sql.text('SELECT Bar,Day,concat(case when Shift_Start>12 then Shift_Start-12 else Shift_Start end,"pm") as Shift_Start, \
            case when Shift_Start <24 then concat(Shift_End-12,"pm") when Shift_Start =24 then "12am" else concat(Shift_End-24,"am") end as Shift_End, \
            case when Day="Monday" then 1 when Day="Tuesday" then 2 when Day="Wednesday" then 3 when Day="Thursday" then 4 \
            when Day="Friday" then 5 when Day="Saturday" then 6 when Day="Sunday" then 7 end as sort  \
            FROM Works WHERE Bartender=:name order by sort;')
        rs=con.execute(query,name=bartender_name)
        return [dict(row) for row in rs]

def get_beers_sold_by_bartender(bartender_name):
    """Gets a list of beers and amount of them sold by each bartender."""

    with engine.connect() as con:
        query=sql.text('SELECT B.Item as Beer,sum(B.Quantity) as Number_Sold \
                FROM Buys B,Transaction_Bills T,Items I \
                WHERE T.Bartender=:name and T.TransactionID=B.TransactionID and T.Bar=B.Bar and \
                B.Item=I.Name and I.Type="Beer" \
                group by Beer;')
        rs=con.execute(query,name=bartender_name)
        results = [dict(row) for row in rs]
        for r in results:
                r['Number_Sold'] = int(r['Number_Sold'])
        return results        

def get_shifts_for_bar_day(bar_name,day):
    """Gets all possible shifts given a bar and day."""

    with engine.connect() as con:
        query=sql.text('SELECT distinct concat(concat(case when Shift_Start>12 then Shift_Start-12 else Shift_Start end,"pm"),"-", \
		    case when Shift_End <24 then concat(Shift_End-12,"pm") when Shift_End = 24 then "12am" else concat(Shift_End-24,"am") end ) as Shift, \
            Shift_Start FROM Works WHERE Bar=:name1 and Day=:name2;')
        rs=con.execute(query,name1=bar_name,name2=day)
        return [dict(row) for row in rs]

def get_bartender_rank_by_beers_sold(bar_name,day,shift_start):
    """Rank the bartenders by total number of beers sold."""

    with engine.connect() as con:
        query=sql.text('SELECT T.Bartender,sum(B.Quantity) Beer_Sold \
            FROM Transaction_Bills T join Buys B \
            on T.Bar=:name1  and T.TransactionID=B.TransactionID and T.Bar=B.Bar \
            and dayname(case when hour(T.TransactionTime) < 8 then date_add(TransactionTime, interval -1 day) else TransactionTime end) = :name2 \
            join Items I on B.Item=I.Name and I.Type="Beer" \
            join Works W on T.Bar = W.Bar \
            and binary(dayname(case when hour(T.TransactionTime) < 8 then date_add(TransactionTime, interval -1 day) else TransactionTime end)) = binary(W.Day) \
            and W.Shift_Start = :name3 \
            GROUP BY T.Bartender;')
        rs=con.execute(query,name1=bar_name,name2=day,name3=shift_start)
        results = [dict(row) for row in rs]
        for r in results:
                r['Beer_Sold'] = int(r['Beer_Sold'])
        return results

def exec_modsql(modsql):
    with engine.connect() as con:
        # execute a modification query
        query = sql.text(modsql)
        # Begin transaction
        trans = con.begin()
        result = con.execute(query)
        message = {'Sql exec succeeded. Number of rows affected': str(result.rowcount)}
        trans.commit()
        # Close connection
        con.close()
        return message

def get_beer_sales_timedistribution(beer):
    with engine.connect() as con:
        # find sales of a beer brand in all weekday / two-hour time intervals
        query = sql.text('select d.Intervals, sum(b.Quantity) Number_Sold \
        from Transaction_Bills a join Buys b on b.Item = :beer and a.TransactionID = b.TransactionID and a.Bar = b.Bar \
        join DayTimeInterval d \
        on binary(dayname(case when hour(a.TransactionTime) < 8 then date_add(a.TransactionTime, interval -1 day) else a.TransactionTime end)) = binary(d.Day) \
        and date_format(a.TransactionTime, "%H:%m:%s") between d.StartTime and d.EndTime \
        group by d.Intervals order by d.Sort; ')
        rs = con.execute(query, beer=beer)
        results = [dict(row) for row in rs]
        if results is None:
                 return None
        for r in results:
            r['Number_Sold'] = float(r['Number_Sold'])
        return results

def exec_selsql(selsql):
    with engine.connect() as con:
        # execute a select query
        query = sql.text(selsql)
        rs = con.execute(query)
        results = [dict(row) for row in rs]
        str_response = ''
        for i, _ in enumerate(results):
            str_response = str_response + '\n' + str(results[i]) 
        return str_response  

def get_top10bars_bybeerday(beer, inputDay):
    with engine.connect() as con:
        # find top ten bars selling a beer brand on a given day, on the beer details page
        query = sql.text('SELECT t.Bar, sum(b.Quantity) as Count \
        FROM Transaction_Bills t, Buys b \
        WHERE t.TransactionID = b.TransactionID AND t.Bar = b.Bar AND b.Item = :beer \
        AND binary(dayname(case when hour(t.TransactionTime) < 8 then date_add(TransactionTime, interval -1 day) else TransactionTime end)) = :inputDay \
        GROUP BY t.Bar ORDER BY 2 desc LIMIT 10;')
        rs = con.execute(query, beer=beer, inputDay=inputDay)
        results = [dict(row) for row in rs]
        for r in results:
                r['Count'] = int(r['Count'])
        return results

def get_top10bars_bybeer(beer):
    with engine.connect() as con:
        # find the top ten bars selling a beer brand, on the bars page
        query = sql.text('SELECT t.Bar, sum(b.Quantity) as Count \
        FROM Transaction_Bills t, Buys b \
        WHERE t.TransactionID = b.TransactionID and t.Bar = b.Bar AND b.Item = :beer \
        GROUP BY t.Bar \
        ORDER BY 2 desc LIMIT 10;')
        rs = con.execute(query, beer=beer)
        results = [dict(row) for row in rs]
        for r in results:
                r['Count'] = int(r['Count'])        
        return results

def get_beer_names():
    """Gets a list of all bar names."""

    with engine.connect() as con:
        query=sql.text('SELECT Name as Beer FROM Items WHERE Type = "Beer";')
        rs=con.execute(query)
        return [row['Beer'] for row in rs]

def get_fraction_by_weekday(bar):
    with engine.connect() as con:
        # count total inventory for the week
        invQuery = sql.text('SELECT sum(s.Inventory) as Inventory \
            FROM Sells s \
            WHERE s.Bar = :bar \
            GROUP BY s.Bar')
        inventory = con.execute(invQuery, bar=bar)
        invRes = [dict(row) for row in inventory]

        # get all transaction times and quantities sold from this bar
        soldQuery = sql.text('SELECT t.TransactionTime, b.Quantity, b.Item \
            FROM Buys b, Transaction_Bills t  \
            WHERE b.Bar = :bar AND t.Bar = b.Bar AND b.TransactionID = t.TransactionID') #counts quantity of item
        sold = con.execute(soldQuery, bar=bar)
        allSold = [dict(row) for row in sold]

        dayTransactions = [0, 0, 0, 0, 0, 0, 0]
        for i in range(len(allSold)):
            day = allSold[i]['TransactionTime'].weekday()
            dayTransactions[day] += allSold[i]['Quantity']

        for i in range(7):
            dayTransactions[i] = float(dayTransactions[i]/invRes[0]['Inventory'])

        return dayTransactions

def get_top10drinkers_bybeer(beer):
    with engine.connect() as con:
        # find top ten drinkers of a beer brand
        query = sql.text('SELECT t.Drinker, sum(b.Quantity) as Count \
        FROM Buys b, Transaction_Bills t \
        WHERE b.TransactionID = t.TransactionID AND t.Bar=b.Bar AND b.Item = :beer \
        GROUP BY t.Drinker \
        ORDER BY 2 desc LIMIT 10')
        rs = con.execute(query, beer=beer)
        results = [dict(row) for row in rs]
        for r in results:
                r['Count'] = int(r['Count'])
        return results

def get_top10cities_byManfRevenue(manf):
    with engine.connect() as con:
        # get top ten cities that has the highest revenue of selling manufacturer's beers
        query = sql.text('SELECT concat(bar.City, ", ", bar.State) CityState, sum(s.Price*buy.Quantity) as Revenue \
        FROM Transaction_Bills t join Buys buy on t.TransactionID = buy.TransactionID and t.Bar = buy.Bar \
        join Items i on buy.Item = i.Name and i.Type = "Beer" and i.Manf = :manf \
        join Bars bar on t.Bar = bar.Name join Sells s on buy.Bar = s.Bar and buy.Item = s.Item \
        where t.TransactionTime between "2018-09-24 12:00" and "2018-10-01 02:00" \
        group by bar.City, bar.State order by 2 desc limit 10;')
        rs = con.execute(query, manf=manf)
        results = [dict(row) for row in rs]
        for r in results:
            r['Revenue'] = float(r['Revenue'])
        return results

def get_top10cities_byManfQuantity(manf):
    with engine.connect() as con:
        # get top ten cities that sold the most number of beers from a manufacturer
        query = sql.text('SELECT concat(bar.City, ", ", bar.State) CityState, sum(buy.Quantity) as Quantity \
        FROM Transaction_Bills t join Buys buy on t.TransactionID = buy.TransactionID and t.Bar = buy.Bar \
        join Items i on buy.Item = i.Name and i.Type = "Beer" and i.Manf = :manf \
        join Bars bar on t.Bar = bar.Name \
        where t.TransactionTime between "2018-09-24 12:00" and "2018-10-01 02:00" \
        group by bar.City, bar.State order by 2 desc limit 10;')
        
        rs = con.execute(query, manf=manf)
        results = [dict(row) for row in rs]
        for r in results:
            r['Quantity'] = int(r['Quantity'])
        return results

def get_manf_topregion_bylikes(manf):
    
    with engine.connect() as con:
        # find top ten cities where more drinkers liked the manufacturer's beers
        query = sql.text('select concat(d.City, ", ", d.State) CityState, count(l.Drinker) Likes \
        from Likes l join Items i on l.Beer = i.Name and i.Manf = :manf \
        join Drinkers d on l.Drinker = d.Name \
        group by d.City, d.State \
        order by 2 desc limit 10 ;;')
        rs = con.execute(query, manf=manf)
        results = [dict(row) for row in rs]
        for r in results:
            r['Likes'] = int(r['Likes'])
        return results

def getmanufacturers():
    with engine.connect() as con:
        query = sql.text('SELECT DISTINCT it.Manf \
        FROM Items it WHERE Type = "Beer" ')
        rs = con.execute(query)
        return [dict(row) for row in rs]

def get_top10bars_bysales(beer, inputDay):
    with engine.connect() as con:
        # find top ten bars selling a beer brand on a given day, on beer detail page
        query = sql.text('SELECT t.Bar, sum(b.Quantity) as Count \
        FROM Transaction_Bills t, Buys b \
        WHERE t.TransactionID = b.TransactionID AND t.Bar = b.Bar AND b.Item = :beer \
        AND binary(dayname(case when hour(t.TransactionTime) < 8 then date_add(TransactionTime, interval -1 day) else TransactionTime end)) = :inputDay \
        GROUP BY t.Bar ORDER BY 2 desc LIMIT 10;')
        rs = con.execute(query, beer=beer, inputDay=inputDay)
        results = [dict(row) for row in rs]
        for r in results:
                r['Count'] = int(r['Count'])
        return results
