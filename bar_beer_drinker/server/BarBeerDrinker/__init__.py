from flask import Flask
from flask import jsonify
from flask import make_response
from flask import request
import json

from BarBeerDrinker import database

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello World!'

@app.route('/api/bar', methods=["GET"])
def get_bars():
    return jsonify(database.get_bars())

@app.route('/api/bar/<name>', methods=["GET"])
def find_bar(name):
    try:
        if name is None:
            raise ValueError("Bar is not specified.")
        bar = database.find_bar(name)
        if bar is None:
            return make_response("No bar found with the given name.", 404)
        return jsonify(bar)
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/menu/<name>', methods=['GET'])
def get_menu(name):
    try:
        if name is None:
            raise ValueError('Bar is not specified.')
        bar = database.find_bar(name)
        if bar is None:
            return make_response("No bar found with the given name.", 404)
        return jsonify(database.get_bar_menu(name))
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/bar-top10beer1', methods=["POST"])
def get_bar_top10beer1():
    body = json.loads(request.data)
    bar = body['bar']
    dw = body['dw']
    return jsonify(database.get_bar_top10beers_byday(bar,dw))

@app.route('/api/bar-top10beers/<bar>', methods=["GET"])
def get_bar_top10beers(bar): 
    try:   
        return jsonify(database.get_bar_top10beers(bar))
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/bar-top10beers/<bar>/<dw>', methods=["GET"])
def get_bar_top10beers_byday(bar,dw): 
    try:   
        return jsonify(database.get_bar_top10beers_byday(bar,dw))
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/bar-top10drinkers/<bar>', methods=["GET"])
def get_bar_top10drinkers(bar): 
    try:   
        return jsonify(database.get_bar_top10drinkers(bar))
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/bar-sales-timedistribution/<bar>', methods=["GET"])
def get_bar_sales_timedistribution(bar): 
    try:   
        return jsonify(database.get_bar_sales_timedistribution(bar))
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/beers_cheaper_than', methods=["POST"])
def find_beers_cheaper_than():
    body = json.loads(request.data)
    max_price = body['maxPrice']
    return jsonify(database.filter_beers(max_price))

@app.route("/api/beer", methods=["GET"])
def get_beers():
    try:
        return jsonify(database.get_beers())
    except Exception as e:
        return make_response(str(e), 500)

@app.route("/api/beer-manufacturer", methods=["GET"])
def get_beer_manufacturers():
    try:
        return jsonify(database.get_beer_manufacturers(None))
    except Exception as e:
        return make_response(str(e), 500)

@app.route("/api/beer-manufacturer/<beer>", methods=["GET"])
def get_manufacturers_making(beer):
    try:
        return jsonify(database.get_beer_manufacturers(beer))
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/bars-selling/<beer>', methods=['GET'])
def find_bars_selling(beer):
    try:
        if beer is None:
            raise ValueError('Beer not specified')
        return jsonify(database.get_bars_selling(beer))
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route("/api/drinker", methods=["GET"])
def get_drinkers():
    try:
        return jsonify(database.get_drinkers())
    except Exception as e:
        return make_response(str(e), 500)

@app.route("/api/drinker/<name>", methods=["GET"])
def get_drinker(name):
    try:
        if name is None:
            raise ValueError("Drinker is not specified.")
        return jsonify(database.get_drinker_info(name))
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route("/api/drinker-transactions/<drinker>", methods=["GET"])
def get_drinker_transactions(drinker):
    try:
        if drinker is None:
            raise ValueError("Drinker is not specified.")
        return jsonify(database.get_drinker_transactions(drinker))
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route("/api/drinker-top10beer/<drinker>", methods=["GET"])
def get_drinker_top10beer(drinker):
    try:
        if drinker is None:
            raise ValueError("Drinker is not specified.")
        return jsonify(database.get_drinker_top10beer(drinker))
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route("/api/drinker-bars/<drinker>", methods=["GET"])
def get_drinker_bars(drinker):
    try:
        if drinker is None:
            raise ValueError("Drinker is not specified.")
        return jsonify(database.get_drinker_bars(drinker))
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route("/api/drinker-bar-byday/<drinker>/<bar>", methods=["GET"])
def get_drinker_bar_byday(drinker,bar):
    try:
        if drinker is None:
            raise ValueError("Drinker is not specified.")
        return jsonify(database.get_drinker_bar_byday(drinker,bar))
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route("/api/drinker-bar-byweek/<drinker>/<bar>", methods=["GET"])
def get_drinker_bar_byweek(drinker,bar):
    try:
        if drinker is None:
            raise ValueError("Drinker is not specified.")
        return jsonify(database.get_drinker_bar_byweek(drinker,bar))
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route("/api/drinker-bar-bymonth/<drinker>/<bar>", methods=["GET"])
def get_drinker_bar_bymonth(drinker,bar):
    try:
        if drinker is None:
            raise ValueError("Drinker is not specified.")
        return jsonify(database.get_drinker_bar_bymonth(drinker,bar))
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route("/api/bar-cities", methods=["GET"])
def get_bar_cities():
    try:
        return jsonify(database.get_bar_cities())
    except Exception as e:
        return make_response(str(e), 500)

@app.route("/api/days-of-week", methods=["GET"])
def get_days_of_week():
    try:
        return jsonify(database.get_days_of_week())
    except Exception as e:
        return make_response(str(e), 500)

@app.route("/api/likes", methods=["GET"])
def get_likes():
    try:
        drinker = request.args.get("drinker")
        if drinker is None:
            raise ValueError("Drinker is not specified.")
        return jsonify(database.get_likes(drinker))
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/frequents-data', methods=['GET'])
def get_bar_frequent_counts():
    try:
        return jsonify(database.get_bar_frequent_counts())
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/bar_names', methods=['GET'])
def get_bar_names():
    try:
        return jsonify(database.get_bar_names())
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/bartender_names/<bar>', methods=['GET'])
def get_bartender_names(bar):
    try:
        if bar is None:
            raise ValueError('Bar not specified')
        return jsonify(database.get_bartender_names(bar))
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/bartender_shifts/<bartender>', methods=['GET'])
def get_bartender_shifts(bartender):
    try:
        if bartender is None:
            raise ValueError('Bartender not specified')
        return jsonify(database.get_bartender_shifts(bartender))
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/beers_sold_by_bartender/<bartender>', methods=['GET'])
def get_beers_sold_by_bartender(bartender):
    try:
        if bartender is None:
            raise ValueError('Bartender not specified')
        return jsonify(database.get_beers_sold_by_bartender(bartender))
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route("/api/shifts_for_bar_day/<bar>/<day>", methods=["GET"])
def get_shifts_for_bar_day(bar,day):
    try:
        if bar is None:
            raise ValueError("Bar is not specified.")
        if day is None:
            raise ValueError("Day is not specified.")
        return jsonify(database.get_shifts_for_bar_day(bar,day))
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route("/api/bartender_rank/<bar>/<day>/<shift_start>", methods=["GET"])
def bartender_rank(bar,day,shift_start):
    try:
        if bar is None:
            raise ValueError("Bar is not specified.")
        if day is None:
            raise ValueError("Day is not specified.")
        if shift_start is None:
            raise ValueError("Shift start is not specified.")
        return jsonify(database.get_bartender_rank_by_beers_sold(bar,day,shift_start))
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/modification', methods=['POST'])
def exec_modsql():
    try:
        body = json.loads(request.data)
        mod_sql = body['modSql']
        return jsonify(database.exec_modsql(mod_sql))
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/beer-sales-timedistribution/<beer>', methods=["GET"])
def get_beer_sales_timedistribution(beer): 
    try:   
        return jsonify(database.get_beer_sales_timedistribution(beer))
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/sql_query_interface', methods=['POST'])
def exec_selsql():
    try:
        body = json.loads(request.data)
        sel_sql = body['selSql']
        return database.exec_selsql(sel_sql)
    except Exception as e:
        return make_response(str(e), 500)

@app.route("/api/top10BarsByBeer/<beer>", methods=["GET"])
def get_top10bars_bybeer(beer):
   try:
       return jsonify(database.get_top10bars_bybeer(beer))
   except Exception as e:
       return make_response(str(e), 500)

@app.route("/api/top10BarsByBeerDay/<beer>/<day>", methods=["GET"])
def get_top10bars_bybeerday(beer, day):
    try:
        if beer is None:
            raise ValueError("Beer is not specified.")
        if day is None:
            raise ValueError("Day is not specified.")
        return jsonify(database.get_top10bars_bybeerday(beer, day))
    except Exception as e:
        return make_response(str(e), 500)

@app.route("/api/top10DrinkersByBeer/<beer>", methods=["GET"])
def get_top10drinkers_bybeer(beer):
    try:
        return jsonify(database.get_top10drinkers_bybeer(beer))
    except Exception as e:
        return make_response(str(e), 500)

@app.route("/api/top10CitiesByManfRevenue/<manf>", methods=["GET"])
def get_top10drinkers_bymanfrevenue(manf):
    try:
        return jsonify(database.get_top10cities_byManfRevenue(manf))
    except Exception as e:
        return make_response(str(e), 500)

@app.route("/api/top10CitiesByManfQuantity/<manf>", methods=["GET"])
def get_top10drinkers_bymanfquantity(manf):
    try:
        return jsonify(database.get_top10cities_byManfQuantity(manf))
    except Exception as e:
        return make_response(str(e), 500)

@app.route("/api/manf_topregion_bylikes/<manf>", methods=["GET"])
def get_manf_topregion_bylikes(manf):
    try:
        if manf is None:
            raise ValueError("Manufacturer is not specified.")
        return jsonify(database.get_manf_topregion_bylikes(manf))
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route("/api/getManufacturers", methods=["GET"])
def getmanufacturers():
    try:
        return jsonify(database.getmanufacturers())
    except Exception as e:
        return make_response(str(e), 500)

@app.route("/api/inventoryFracByWeekDay/<bar>", methods=["GET"])
def get_fract_by_weekday(bar):
    try:
        if bar is None:
            raise  ValueError("Bar is not specified.")
        return jsonify(database.get_fraction_by_weekday(bar))
    except Exception as e:
        return make_response(str(e), 500)

@app.route("/api/top10BarsBySales/<beer>/<inputDay>", methods=["GET"])
def get_top10bars_bysales(beer,inputDay):
    try:
        if beer is None:
            raise ValueError("Beer is not specified.")
        if inputDay is None:
            raise ValueError("Day is not specified.")
        return jsonify(database.get_top10bars_bysales(beer,inputDay))
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/beer-names', methods=['GET'])
def get_beer_names():
    try:
        return jsonify(database.get_beer_names())
    except Exception as e:
        return make_response(str(e), 500)




