import { Component, OnInit } from '@angular/core';
import { BeersService, BeerLocation } from '../beers.service';
import { ActivatedRoute } from '@angular/router';

import { SelectItem } from 'primeng/components/common/selectitem';

declare const Highcharts: any;

@Component({
  selector: 'app-beer-details',
  templateUrl: './beer-details.component.html',
  styleUrls: ['./beer-details.component.css']
})
export class BeerDetailsComponent implements OnInit {

  beerName: string;
  beerLocations: BeerLocation[];
  manufacturer: string;

  filterOptions: SelectItem[];
  sortField: string;
  sortOrder: number;

  constructor(
    private beerService: BeersService,
    private route: ActivatedRoute
  ) {
    this.route.paramMap.subscribe((paramMap) => {
      this.beerName = paramMap.get('beer');

      this.beerService.getBarsSelling(this.beerName).subscribe(
        data => {
          this.beerLocations = data;
        }
      );

      this.beerService.getBeerManufacturers(this.beerName)
        .subscribe(
          data => {
            this.manufacturer = data;
          }
        );

      this.filterOptions = [
        {
          'label': 'Low price first',
          'value': 'low price'
        },
        {
          'label': 'High price first',
          'value': 'high price'
        },
        {
          'label': 'Most inventory first',
          'value': 'high inventory'
        },
        {
          'label': 'Least inventory first',
          'value': 'low inventory'
        }
      ];
    });

    this.beerService.getBeerSalesTimeDistribution(this.beerName).subscribe(
      data => {
        console.log(data);

        const intervals = [];
        const sales = [];

        data.forEach(interval => {
          intervals.push(interval.Intervals);
          sales.push(interval.Number_Sold);
        });

        this.renderTimeDistributionChart(intervals, sales);
      }
    );

    this.beerService.getTop10BarsByBeer(this.beerName).subscribe(
      data => {
        const names = [];
        const count = [];
        data.forEach( d => {
          names.push(d.Bar);
          count.push(d.Count);
        });
        this.renderChart2(names, count);
      }
    );

    this.beerService.getTop10DrinkersByBeer(this.beerName).subscribe(
      data => {
        const names = [];
        const count = [];
        data.forEach( d => {
          names.push(d.Drinker);
          count.push(d.Count);
        });
        this.renderChart(names, count);
      }
    );
  }

  ngOnInit() {
  }

  renderChart(names: string[], counts: number[]) {
    Highcharts.chart('bargraph_drinkers', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: names,
        title: {
          text: 'Drinker'
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Number of Beers Bought'
        },
        labels: {
          overflow: 'justify'
        }
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        enabled: false
      },
      credits: {
        enabled: false
      },
      series: [{
        data: counts
      }]
    });
  }
  renderTimeDistributionChart(intervals: string[], sales: number[]) {
    Highcharts.chart('bargraph_timedistribution', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: intervals,
        title: {
          text: 'Day/Time Interval'
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Number of Beers Sold'
        },
        labels: {
          overflow: 'justify'
        }
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        enabled: false
      },
      credits: {
        enabled: false
      },
      series: [{
        data: sales
      }]
    });
  }

  renderChart2(names: string[], counts: number[]) {
    Highcharts.chart('bargraph_bars', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: names,
        title: {
          text: 'Bar'
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Number of Beers Sold'
        },
        labels: {
          overflow: 'justify'
        }
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        enabled: false
      },
      credits: {
        enabled: false
      },
      series: [{
        data: counts
      }]
    });
  }

  sortBy(selectedOption: string) {
    if (selectedOption === 'low price') {
      this.beerLocations.sort((a, b) => {
        return a.Price - b.Price;
      });
    } else if (selectedOption === 'high price') {
      this.beerLocations.sort((a, b) => {
        return b.Price - a.Price;
      });
    } else if (selectedOption === 'low inventory') {
      this.beerLocations.sort((a, b) => {
        return a.Inventory - b.Inventory;
      });
    } else if (selectedOption === 'high inventory') {
      this.beerLocations.sort((a, b) => {
        return b.Inventory - a.Inventory;
      });
    }
  }



}
