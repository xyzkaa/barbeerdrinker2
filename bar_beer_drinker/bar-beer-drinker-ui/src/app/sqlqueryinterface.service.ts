import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class SqlqueryinterfaceService {
  constructor(
    public http: HttpClient
  ) { }

  postSqlqueryinterface(sqlStmt_str) {
    const options = {responseType: 'text' as 'text'};
    return this.http.post('/api/sql_query_interface', {'selSql' : sqlStmt_str}, options);
  }

}
