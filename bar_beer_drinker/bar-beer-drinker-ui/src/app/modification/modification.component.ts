import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModificationService } from '../modification.service';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-modification',
  templateUrl: './modification.component.html',
  styleUrls: ['./modification.component.css']
})

export class ModificationComponent implements OnInit {

  rspBarsMessage = '';
  rspDrinkersMessage = '';
  rspItemsMessage = '';
  rspBartendersMessage = '';
  rspTransaction_BillsMessage = '';
  rspBar_HoursMessage = '';
  rspWorksMessage = '';
  rspBuysMessage = '';
  rspFrequentsMessage = '';
  rspLikesMessage = '';
  rspSellsMessage = '';

  constructor(
    private modService: ModificationService,
    private route: ActivatedRoute
  ) {
    }

  ngOnInit() {
  }

  onKey(event: any) {
    // this.rspBarsMessage = '';
  }

  run_bars_sql(str_sqlStmt: string) {
    this.rspBarsMessage = '';
    this.modService.postModification(str_sqlStmt).subscribe(
      data => {
        console.log(data);
        this.rspBarsMessage = 'Sql Execution succeeded.';
      },
      (error: HttpErrorResponse) => {
        this.rspBarsMessage = 'error = ' + error.error;
        console.error(error.status + ' - ' + error.error);
        /* alert('An error occured on the server. Please check the browser console.'); */
      }
    );
  }

  run_drinkers_sql(str_sqlStmt: string) {
    this.rspDrinkersMessage = '';
    this.modService.postModification(str_sqlStmt).subscribe(
      data => {
        console.log(data);
        this.rspDrinkersMessage = 'Sql Execution succeeded.';
      },
      (error: HttpErrorResponse) => {
        this.rspDrinkersMessage = 'error = ' + error.error;
        console.error(error.status + ' - ' + error.error);
        /* alert('An error occured on the server. Please check the browser console.'); */
      }
    );
  }

  run_items_sql(str_sqlStmt: string) {
    this.rspItemsMessage = '';
    this.modService.postModification(str_sqlStmt).subscribe(
      data => {
        console.log(data);
        this.rspItemsMessage = 'Sql Execution succeeded.';
      },
      (error: HttpErrorResponse) => {
        this.rspItemsMessage = 'error = ' + error.error;
        console.error(error.status + ' - ' + error.error);
        /* alert('An error occured on the server. Please check the browser console.'); */
      }
    );
  }

  run_bartenders_sql(str_sqlStmt: string) {
    this.rspBartendersMessage = '';
    this.modService.postModification(str_sqlStmt).subscribe(
      data => {
        console.log(data);
        this.rspBartendersMessage = 'Sql Execution succeeded.';
      },
      (error: HttpErrorResponse) => {
        this.rspBartendersMessage = 'error = ' + error.error;
        console.error(error.status + ' - ' + error.error);
        /* alert('An error occured on the server. Please check the browser console.'); */
      }
    );
  }

  run_transaction_bills_sql(str_sqlStmt: string) {
    this.rspTransaction_BillsMessage = '';
    this.modService.postModification(str_sqlStmt).subscribe(
      data => {
        console.log(data);
        this.rspTransaction_BillsMessage = 'Sql Execution succeeded.';
      },
      (error: HttpErrorResponse) => {
        this.rspTransaction_BillsMessage = 'error = ' + error.error;
        console.error(error.status + ' - ' + error.error);
        /* alert('An error occured on the server. Please check the browser console.'); */
      }
    );
  }

  run_bar_hours_sql(str_sqlStmt: string) {
    this.rspBar_HoursMessage = '';
    this.modService.postModification(str_sqlStmt).subscribe(
      data => {
        console.log(data);
        this.rspBar_HoursMessage = 'Sql Execution succeeded.';
      },
      (error: HttpErrorResponse) => {
        this.rspBar_HoursMessage = 'error = ' + error.error;
        console.error(error.status + ' - ' + error.error);
        /* alert('An error occured on the server. Please check the browser console.'); */
      }
    );
  }

  run_works_sql(str_sqlStmt: string) {
    this.rspWorksMessage = '';
    this.modService.postModification(str_sqlStmt).subscribe(
      data => {
        console.log(data);
        this.rspWorksMessage = 'Sql Execution succeeded.';
      },
      (error: HttpErrorResponse) => {
        this.rspWorksMessage = 'error = ' + error.error;
        console.error(error.status + ' - ' + error.error);
        /* alert('An error occured on the server. Please check the browser console.'); */
      }
    );
  }

  run_buys_sql(str_sqlStmt: string) {
    this.rspBuysMessage = '';
    this.modService.postModification(str_sqlStmt).subscribe(
      data => {
        console.log(data);
        this.rspBuysMessage = 'Sql Execution succeeded.';
      },
      (error: HttpErrorResponse) => {
        this.rspBuysMessage = 'error = ' + error.error;
        console.error(error.status + ' - ' + error.error);
        /* alert('An error occured on the server. Please check the browser console.'); */
      }
    );
  }

  run_frequents_sql(str_sqlStmt: string) {
    this.rspFrequentsMessage = '';
    this.modService.postModification(str_sqlStmt).subscribe(
      data => {
        console.log(data);
        this.rspFrequentsMessage = 'Sql Execution succeeded.';
      },
      (error: HttpErrorResponse) => {
        this.rspFrequentsMessage = 'error = ' + error.error;
        console.error(error.status + ' - ' + error.error);
        /* alert('An error occured on the server. Please check the browser console.'); */
      }
    );
  }

  run_likes_sql(str_sqlStmt: string) {
    this.rspLikesMessage = '';
    this.modService.postModification(str_sqlStmt).subscribe(
      data => {
        console.log(data);
        this.rspLikesMessage = 'Sql Execution succeeded.';
      },
      (error: HttpErrorResponse) => {
        this.rspLikesMessage = 'error = ' + error.error;
        console.error(error.status + ' - ' + error.error);
        /* alert('An error occured on the server. Please check the browser console.'); */
      }
    );
  }

  run_sells_sql(str_sqlStmt: string) {
    this.rspSellsMessage = '';
    this.modService.postModification(str_sqlStmt).subscribe(
      data => {
        console.log(data);
        this.rspSellsMessage = 'Sql Execution succeeded.';
      },
      (error: HttpErrorResponse) => {
        this.rspSellsMessage = 'error = ' + error.error;
        console.error(error.status + ' - ' + error.error);
        /* alert('An error occured on the server. Please check the browser console.'); */
      }
    );
  }


}

