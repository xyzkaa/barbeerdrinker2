import { Component, OnInit } from '@angular/core';
import { BarsService, Bar } from '../bars.service';
import { SelectItem } from 'primeng/components/common/selectitem';
import { BeersService } from '../beers.service';

declare const Highcharts: any;

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})


export class WelcomeComponent implements OnInit {
  bars: Bar[];
  beerOptions: SelectItem[];
  dayOptions: SelectItem[];
  selectedBeer: string;
  selectedDay: string;

  constructor(
    public barService: BarsService,
    private beerService: BeersService

  ) {
    this.getBars();
    
    this.beerService.getBeersName().subscribe(
      data => {
        this.beerOptions = data.map(Beer => {
          return {
            label: Beer,
            value: Beer,
          };
        }
        );
      }
    );
    this.barService.getDaysOfWeek().subscribe(
      data => {
        this.dayOptions = data.map(Day => {
          return {
            label: Day,
            value: Day,
          };
        }
        );
      }
    );
  

  }
  ngOnInit() {
  }
  getBars() {
    this.barService.getBars().subscribe(
      data => {
        this.bars = data;
      },
      error => {
        alert('Could not retrieve a list of bars');
      }
    );
  }

  saveBeers(thisBeer: string) {
    this.selectedBeer = thisBeer;
    if (this.selectedDay) {
      this.getSales();
    }
   }

   saveDays(thisDay: string) {
    this.selectedDay = thisDay;
    if (this.selectedBeer) {
      this.getSales();
    }
   }

   getSales() {
    this.barService.getTop10BarsBySales(this.selectedBeer, this.selectedDay).subscribe(
      data => {
        console.log(data);
        const bars = [];
        const count = [];
        data.forEach( d => {
          bars.push(d.Bar);
          count.push(d.Count);
        });
        this.renderChart(bars, count);
      });
   }

   renderChart(bars: string[], count: number[]) {
    Highcharts.chart('bargraph_top10barsbysales', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: bars,
        title: {
          text: 'Bar'
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Total Beers Sold'
        },
        labels: {
          overflow: 'justify'
        }
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        enabled: false
      },
      credits: {
        enabled: false
      },
      series: [{
        data: count
      }]
    });
   }

}
