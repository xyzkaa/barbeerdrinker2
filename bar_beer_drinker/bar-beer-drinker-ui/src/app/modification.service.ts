import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ModificationService {
  constructor(
    public http: HttpClient
  ) { }

  postModification(sqlStmt_str) {
    return this.http.post('/api/modification', {'modSql' : sqlStmt_str});
  }

}

