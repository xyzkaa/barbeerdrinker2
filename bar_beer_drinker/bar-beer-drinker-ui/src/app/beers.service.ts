import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface BeerLocation {
  Bar: string;
  Price: number;
  Inventory: number;
}

@Injectable({
  providedIn: 'root'
})
export class BeersService {

  constructor(private http: HttpClient) { }

  getBeers() {
    return this.http.get<any[]>('/api/beer');
  }

  getBarsSelling(beer: string) {
    return this.http.get<BeerLocation[]>(`/api/bars-selling/${beer}`);
  }

  getBeerManufacturers(beer?: string): any {
    if (beer) {
      return this.http.get<string>(`/api/beer-manufacturer/${beer}`);
    }
    return this.http.get<string[]>('/api/beer-manufacturer');
  }

  getBeerSalesTimeDistribution(beer: string) {
    return this.http.get<any[]>('/api/beer-sales-timedistribution/' + beer);
  }

  getTop10BarsByBeer(beer: string) {
    return this.http.get<any[]>('/api/top10BarsByBeer/' + beer);
  }

  getTop10DrinkersByBeer(beer: string) {
    return this.http.get<any[]>('/api/top10DrinkersByBeer/' + beer);
  }

  getTop10CitiesByManfRevenue(manf: string) {
      return this.http.get<any[]>('/api/top10CitiesByManfRevenue/' + manf);
  }

  getTop10CitiesByManfQuantity(manf: string) {
      return this.http.get<any[]>('/api/top10CitiesByManfQuantity/' + manf);
  }

  get_manf_topregion_bylikes(manf: string) {
    return this.http.get<any[]>('/api/manf_topregion_bylikes/' + manf);
  }

  getManufacturers() {
    return this.http.get<any[]>('/api/getManufacturers');
  }

  getBeersName() {
    return this.http.get<string[]>('/api/beer-names');
  }

}

