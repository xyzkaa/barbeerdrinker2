import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BeersService } from '../beers.service';
import { HttpResponse } from '@angular/common/http';
import { SelectItem } from 'primeng/components/common/selectitem';

declare const Highcharts: any;

@Component({
  selector: 'app-manufacturers',
  templateUrl: './manufacturers.component.html',
  styleUrls: ['./manufacturers.component.css']
})
export class ManufacturersComponent implements OnInit {

  top10CitiesPrice: string[];
  top10CitiesQuantity: string[];
  manfOptions: SelectItem[];
  selectedManf: string;
  manf: string;

  constructor(
    private beerService: BeersService,
    private route: ActivatedRoute
  ) {
    this.beerService.getBeerManufacturers().subscribe(
      data => {
        this.manfOptions = data.map(Manf => {
          return {
            label: Manf,
            value: Manf,
          };
        }
        );
      }

    );

    }
    ngOnInit() {
    }

    showManufacturersData(manf: string) {
      this.manf = manf;
      this.beerService.get_manf_topregion_bylikes(manf).subscribe( data => {
        const citystates2 = [];
        const likes = [];

        data.forEach( citystate2 => {

          citystates2.push(citystate2.CityState);
          likes.push(citystate2.Likes);
        });
        this.renderManfChartByQuantity(citystates2, likes);
      });

      this.beerService.getTop10CitiesByManfRevenue(manf).subscribe(
        data => {
          console.log(data);
          const citystates = [];
          const revenue = [];

          data.forEach(citystate => {
            citystates.push(citystate.CityState);
            revenue.push(citystate.Revenue);
          });
          this.renderManfChart(citystates, revenue);
        }

      );


    }
    /*beerService.getBeerManufacturers().subscribe(
      data => {
        this.manfOptions = data.map(Manf => {
          return {
            label: Manf,
            value: Manf,
          };
        }
        );
      }

    )*/
    renderManfChart(cityState: string[], revenue: number[]) {
      Highcharts.chart('bargraph_cityRevenue', {
        chart: {
          type: 'column'
        },
        title: {
          text: 'Top Ten City/States that Generated the Most Revenue from ' + this.manf  + ' Products Last Week'
        },
        xAxis: {
          categories: cityState,
          title: {
            text: 'City/State'
          }
        },
        yAxis: {
          min: 0,
          title: {
            text: 'Revenue ($)'
          },
          labels: {
            overflow: 'justify'
          }
        },
        plotOptions: {
          bar: {
            dataLabels: {
              enabled: true
            }
          }
        },
        legend: {
          enabled: false
        },
        credits: {
          enabled: false
        },
        series: [{
          data: revenue
        }]
      });
    }


  renderManfChartByQuantity(cityState2: string[], likes: number[]) {
    Highcharts.chart('bargraph_cityQuantity', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Top Ten City/States That Like ' + this.manf + ' Beers Most'
      },
      xAxis: {
        categories: cityState2,
        title: {
          text: 'City/State'
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Number of Likes'
        },
        labels: {
          overflow: 'justify'
        }
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        enabled: false
      },
      credits: {
        enabled: false
      },
      series: [{
        data: likes
      }]
    });
  }
}

