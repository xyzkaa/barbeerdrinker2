import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SqlqueryinterfaceService } from '../sqlqueryinterface.service';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-sqlqueryinterface',
  templateUrl: './sqlqueryinterface.component.html',
  styleUrls: ['./sqlqueryinterface.component.css']
})

export class SqlqueryinterfaceComponent implements OnInit {

  rspMessage = '';
  selSql: String;
  str_myData: String;

  constructor(
    private modService: SqlqueryinterfaceService,
    private route: ActivatedRoute
  ) {
    }

  ngOnInit() {
  }

  onKey(event: any) {
    // this.rspMessage = '';
    // this.str_myData = '';
  }

  run_sql(str_sqlStmt: string) {
    this.rspMessage = '';
    this.str_myData = '';
    this.modService.postSqlqueryinterface(str_sqlStmt).subscribe(
      data => {
        this.str_myData = data;
        console.log(data);
        this.rspMessage = 'Sql Exection succeeded.';
      },
      (error: HttpErrorResponse) => {
        this.rspMessage = 'error = ' + error.error;
        console.error(error.status + ' - ' + error.error);
        /* alert('An error occured on the server. Please check the browser console.'); */
      }
    );
  }
}
