import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SqlqueryinterfaceComponent } from './sqlqueryinterface.component';

describe('SqlqueryinterfaceComponent', () => {
  let component: SqlqueryinterfaceComponent;
  let fixture: ComponentFixture<SqlqueryinterfaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SqlqueryinterfaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SqlqueryinterfaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
