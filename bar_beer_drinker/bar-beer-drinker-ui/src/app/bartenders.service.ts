import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface BartenderShifts {
  Bar: string;
  Day: string;
  Shift_Start: string;
  Shift_End: string;
  }

export interface Shifts {
    Shift: string;
    Shift_Start: number;
    }

@Injectable({
  providedIn: 'root'
})

export class BartendersService {
  constructor(
    public http: HttpClient
  ) { }

  getBarNames() {
    return this.http.get<string[]>('/api/bar_names');
  }

  getBartenderNames(bar: string) {
    return this.http.get<string[]>('/api/bartender_names/' + bar);
  }

  getBartenderShifts(bartender: string) {
    return this.http.get<BartenderShifts[]>('/api/bartender_shifts/' + bartender);
  }

  getBeersSoldByBartender(bartender: string) {
    return this.http.get<any[]>('/api/beers_sold_by_bartender/' + bartender);
  }

  getShiftsForBarDay(bar: string, day: string) {
    return this.http.get<any[]>('/api/shifts_for_bar_day/' + bar + '/' + day);
  }

  getBartenderRank(bar: string, day: string, shift_start: string) {
    return this.http.get<any[]>('/api/bartender_rank/' + bar + '/' + day + '/' + shift_start);
  }

  getDaysOfWeek() {
    return this.http.get<string[]>('/api/days-of-week');
  }
}