import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BartendersService, BartenderShifts, Shifts } from '../bartenders.service';
import { HttpResponse } from '@angular/common/http';
import { SelectItem } from 'primeng/components/common/selectitem';

declare const Highcharts: any;

@Component({
  selector: 'app-bartenders',
  templateUrl: './bartenders.component.html',
  styleUrls: ['./bartenders.component.css']
})
export class BartendersComponent implements OnInit {

  barOptions1: SelectItem[];
  bartenderOptions: SelectItem[];
  barOptions2: SelectItem[];
  dayOptions: SelectItem[];
  shiftOptions: SelectItem[];
  bartenderShifts: BartenderShifts[];
  shifts: Shifts[];
  selectedBar1: string;
  selectedBartender1: string;
  selectedBartender2: string;
  selectedBar2: string;
  selectedDay: string;
  selectedShift: string;
  display_style: string;


  constructor(
    private bartendersService: BartendersService,
    private route: ActivatedRoute
  ) {
    this.display_style = 'None';
    this.bartendersService.getBarNames().subscribe(
      data => {
        this.barOptions1 = data.map(Bar => {
          return {
            label: Bar,
            value: Bar,
          };
        }
        );
      }
    );
    this.bartendersService.getBarNames().subscribe(
      data => {
        this.barOptions2 = data.map(Bar => {
          return {
            label: Bar,
            value: Bar,
          };
        }
        );
      }
    );

    this.bartendersService.getDaysOfWeek().subscribe(
      data => {
        this.dayOptions = data.map(Day => {
          return {
            label: Day,
            value: Day,
          };
        }
        );
      }
    );



    }
      ngOnInit() {
    }
displayBartenders(Bar: string) {

  this.selectedBar1 = Bar;
  this.bartendersService.getBartenderNames(Bar).subscribe(
    data => {
      this.bartenderOptions = data.map(Bartender => {
        return {
          label: Bartender,
          value: Bartender,
        };
      });
    }
  );
  }

saveBarOption(thisBar: string) {
  this.selectedBar2 = thisBar;
  if (this.selectedDay) {
    this.getShifts();
  }

}
saveDayOption(thisDay: string) {
  this.selectedDay = thisDay;
  if (this.selectedBar2) {
    this.getShifts();
  }

}
getShifts() {

    this.display_style = 'block';
    this.bartendersService.getShiftsForBarDay(this.selectedBar2, this.selectedDay).subscribe(
      data => {
        console.log(data);
        this.shiftOptions = data.map(shifts => {
          return {
            label: shifts.Shift,
            value: shifts.Shift_Start
          };
        });
      }
    );
    }

    showTopBartenders(shift: string) {
      // this.selectedShift = shift;
      this.selectedShift = shift;
      this.bartendersService.getBartenderRank(this.selectedBar2, this.selectedDay, shift).subscribe(
          data => {
            console.log(data);

          const bartenders = [];
          const sold = [];
              data.forEach(bartender => {
              bartenders.push(bartender.Bartender);
              sold.push(bartender.Beer_Sold);
            });

            this.renderTopBartenderChart(bartenders, sold);
          }
        );
      }
      renderTopBartenderChart(bartenders: string[], sold: number[]) {
        Highcharts.chart('bargraph_topbartenders', {
          chart: {
            type: 'column'
          },
          title: {
            text: 'Top Bartenders by Sales (in ' + this.selectedBar2 + ' on ' + this.selectedDay + ' during the selected shift)'
          },
          xAxis: {
            categories: bartenders,
            title: {
              text: 'Bartender'
            }
          },
          yAxis: {
            min: 0,
            title: {
              text: 'Number of Beers Sold'
            },
            labels: {
              overflow: 'justify'
            }
          },
          plotOptions: {
            bar: {
              dataLabels: {
                enabled: true
              }
            }
          },
          legend: {
            enabled: false
          },
          credits: {
            enabled: false
          },
          series: [{
            data: sold
          }]
        });
      }

showBartenderShiftAndSales(Bartender: string) {
    this.selectedBartender1 = Bartender;
    this.bartendersService.getBartenderShifts(Bartender).subscribe(
      data => {
        this.bartenderShifts = data;
      });


    this.bartendersService.getBeersSoldByBartender(Bartender).subscribe(
        data => {
          console.log(data);

          const beers = [];
          const sold = [];

          data.forEach(beer => {
            beers.push(beer.Beer);
            sold.push(beer.Number_Sold);
          });

          this.renderBartenderChart(beers, sold);
        }
      );
    }

    renderBartenderChart(beers: string[], sold: number[]) {
      Highcharts.chart('bargraph_bertendersales', {
        chart: {
          type: 'column'
        },
        title: {
          text: 'Top Beer Brands Sold by ' + this.selectedBartender1
        },
        xAxis: {
          categories: beers,
          title: {
            text: 'Beer'
          }
        },
        yAxis: {
          min: 0,
          title: {
            text: 'Number of Beers Sold'
          },
          labels: {
            overflow: 'justify'
          }
        },
        plotOptions: {
          bar: {
            dataLabels: {
              enabled: true
            }
          }
        },
        legend: {
          enabled: false
        },
        credits: {
          enabled: false
        },
        series: [{
          data: sold
        }]
      });
    }

  }
