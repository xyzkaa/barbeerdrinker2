import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BarsService, Bar, BarMenuItem } from '../bars.service';
import { HttpResponse } from '@angular/common/http';
import { SelectItem } from 'primeng/components/common/selectitem';

declare const Highcharts: any;

@Component({
  selector: 'app-bar-details',
  templateUrl: './bar-details.component.html',
  styleUrls: ['./bar-details.component.css']
})


export class BarDetailsComponent implements OnInit {
  barName: string;
  barDetails: Bar;
  menu: BarMenuItem[];

  filterOptions: SelectItem[];
  sortField: string;
  sortOrder: number;

  dayOptions: SelectItem[];
  weekdays: string[];

  constructor(
    private barService: BarsService,
    private route: ActivatedRoute
  ) {
    route.paramMap.subscribe((paramMap) => {
      this.barName = paramMap.get('bar');
      barService.getBar(this.barName).subscribe(
        data => {
          this.barDetails = data;
        },
        (error: HttpResponse<any>) => {
          if (error.status === 404) {
            alert('Bar not found');
          } else {
            console.error(error.status + ' - ' + error.body);
            alert('An error occured on the server. Please check the browser console.');
          }
        }
      );
      barService.getMenu(this.barName).subscribe(
        data => {
          this.menu = data;
        },
        (error: HttpResponse<any>) => {
          if (error.status === 404) {
            alert('Bar not found');
          } else {
            console.error(error.status + ' - ' + error.body);
            alert('An error occurred on the server. Please check the browser console.');
          }
        }
      );
      this.barService.getBarTop10Drinkers(this.barName).subscribe(
        data => {
          console.log(data);

          const drinkers = [];
          const spents = [];

          data.forEach(drinker => {
            drinkers.push(drinker.Drinker);
            spents.push(drinker.TotalSpending);
          });

          this.renderDrinkerChart(drinkers, spents);
        }
      );

      this.barService.getBarSalesTimeDistribution(this.barName).subscribe(
        data => {
          console.log(data);

          const intervals = [];
          const sales = [];

          data.forEach(interval => {
            intervals.push(interval.Intervals);
            sales.push(interval.TotalSales);
          });

          this.renderTimeDistributionChart(intervals, sales);
        }
      );

      barService.getDaysOfWeek().subscribe(
        data => {
          this.dayOptions = data.map(Day => {
            return {
              label: Day,
              value: Day,
            };
          });
        }
      );
      this.barService.getFractionByWeekDay(this.barName).subscribe(
        data => {
          const fractions = [];
          data.forEach( f => {
            fractions.push(f);
          });

        this.weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday',
        'Saturday', 'Sunday'];
        this.renderFractionsChart(this.weekdays, fractions);
        }

      );

      this.filterOptions = [
        {
          'label': 'Low price first',
          'value': 'low price'
        },
        {
          'label': 'High price first',
          'value': 'high price'
        }
      ];
    });
  }
  sortBy(selectedOption: string) {
    if (selectedOption === 'low price') {
      this.menu.sort((a, b) => {
        return a.Price - b.Price;
      });
    } else if (selectedOption === 'high price') {
      this.menu.sort((a, b) => {
        return b.Price - a.Price;
      });
    }
  }

  ngOnInit() {
  }

  renderChart(beers: string[], counts: number[]) {
    Highcharts.chart('bargraph_beers', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: beers,
        title: {
          text: 'Beer'
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Number of beers sold'
        },
        labels: {
          overflow: 'justify'
        }
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        enabled: false
      },
      credits: {
        enabled: false
      },
      series: [{
        data: counts
      }]
    });
  }

  renderDrinkerChart(drinkers: string[], spents: number[]) {
    Highcharts.chart('bargraph_drinkers', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: drinkers,
        title: {
          text: 'Drinker'
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Amount Spent ($)'
        },
        labels: {
          overflow: 'justify'
        }
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        enabled: false
      },
      credits: {
        enabled: false
      },
      series: [{
        data: spents
      }]
    });
  }
  renderFractionsChart(weekdays: string[], fractions: number[]) {
    Highcharts.chart('bargraph_fractions', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: weekdays,
        title: {
          text: 'Weekday'
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Fraction of inventory sold'
        },
        labels: {
          overflow: 'justify'
        }
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        enabled: false
      },
      credits: {
        enabled: false
      },
      series: [{
        data: fractions
      }]
    });
  }

  renderTimeDistributionChart(intervals: string[], sales: number[]) {
    Highcharts.chart('bargraph_timedistribution', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: intervals,
        title: {
          text: 'Day/Time Interval'
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Total Sales ($)'
        },
        labels: {
          overflow: 'justify'
        }
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        enabled: false
      },
      credits: {
        enabled: false
      },
      series: [{
        data: sales
      }]
    });
  }

  refreshTop10Beer(Day: string) {
    this.barService.getBarTop10BeersByDay(this.barName, Day).subscribe(
      data => {
        console.log(data);

        const beers = [];
        const counts = [];

        data.forEach(beer => {
          beers.push(beer.Beer);
          counts.push(beer.Counts);
        });

        this.renderChart(beers, counts);
      }
    );
}

}

