import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrinkerDetailComponent } from './drinker-details.component';

describe('DrinkerDetailComponent', () => {
  let component: DrinkerDetailComponent;
  let fixture: ComponentFixture<DrinkerDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrinkerDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrinkerDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
