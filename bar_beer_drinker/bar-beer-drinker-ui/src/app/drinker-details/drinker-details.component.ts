import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DrinkersService, Drinker, DrinkerTransaction } from '../drinkers.service';
import { HttpResponse } from '@angular/common/http';
import { SelectItem } from 'primeng/components/common/selectitem';

declare const Highcharts: any;

@Component({
  selector: 'app-drinker-details',
  templateUrl: './drinker-details.component.html',
  styleUrls: ['./drinker-details.component.css']
})
export class DrinkerDetailsComponent implements OnInit {
  drinkerName: string;
  drinkerDetails: Drinker;
  transaction: DrinkerTransaction[];

  barOptions: SelectItem[];

  constructor(
    private drinkersService: DrinkersService,
    private route: ActivatedRoute
  ) {
      route.paramMap.subscribe((paramMap) => {
        this.drinkerName = paramMap.get('drinker');
        drinkersService.getDrinker(this.drinkerName).subscribe(
          data => {
            this.drinkerDetails = data;
          },
          (error: HttpResponse<any>) => {
            if (error.status === 404) {
              alert('Drinker not found');
            } else {
              console.error(error.status + ' - ' + error.body);
              alert('An error occured on the server. Please check the browser console.');
            }
          });
          drinkersService.getDrinkerTransactions(this.drinkerName).subscribe(
            data => {
              this.transaction = data;
            },
            (error: HttpResponse<any>) => {
              if (error.status === 404) {
                alert('Drinker not found');
              } else {
                console.error(error.status + ' - ' + error.body);
                alert('An error occurred on the server. Please check the browser console.');
              }
            }
        );
        this.drinkersService.getDrinkerBars(this.drinkerName).subscribe(
          data => {
            this.barOptions = data.map(Bar => {
              return {
                label: Bar,
                value: Bar,
              };
            });
          }
        );

        this.drinkersService.getDrinkerTop10Beer(this.drinkerName).subscribe(
          data => {
            console.log(data);

            const beers = [];
            const ordqty = [];

            data.forEach(beer => {
              beers.push(beer.Beer);
              ordqty.push(beer.OrdQty);
            });

            this.renderDrinkerTop10BeerChart(beers, ordqty);
          }
        );


        });
      }
      ngOnInit() {
    }
    renderDrinkerTop10BeerChart(beers: string[], ordqty: number[]) {
      Highcharts.chart('bargraph_top10beers', {
        chart: {
          type: 'column'
        },
        title: {
          text: ''
        },
        xAxis: {
          categories: beers,
          title: {
            text: 'Beer'
          }
        },
        yAxis: {
          min: 0,
          title: {
            text: 'Number of beers ordered'
          },
          labels: {
            overflow: 'justify'
          }
        },
        plotOptions: {
          bar: {
            dataLabels: {
              enabled: true
            }
          }
        },
        legend: {
          enabled: false
        },
        credits: {
          enabled: false
        },
        series: [{
          data: ordqty
        }]
      });
    }
    refreshDrinkerBar(Bar: string) {
      this.drinkersService.getDrinkerBarByDay(this.drinkerName, Bar).subscribe(
        data => {
          console.log(data);

          const weekdays = [];
          const spents_day = [];

          data.forEach(weekday => {
            weekdays.push(weekday.WeekDay);
            spents_day.push(weekday.TotalSpending);
          });

          this.renderDrinkerBarByDayChart(weekdays, spents_day);
        }
      );
      this.drinkersService.getDrinkerBarByWeek(this.drinkerName, Bar).subscribe(
        data => {
          console.log(data);

          const weeks = [];
          const spents_week = [];

          data.forEach(week => {
            weeks.push(week.WeekOfYear);
            spents_week.push(week.TotalSpending);
          });

          this.renderDrinkerBarByWeekChart(weeks, spents_week);
        }
      );
      this.drinkersService.getDrinkerBarByMonth(this.drinkerName, Bar).subscribe(
        data => {
          console.log(data);

          const months = [];
          const spents_month = [];

          data.forEach(month => {
            months.push(month.Month);
            spents_month.push(month.TotalSpending);
          });

          this.renderDrinkerBarByMonthChart(months, spents_month);
        }
      );
  }
  renderDrinkerBarByDayChart(weekdays: string[], spents: number[]) {
    Highcharts.chart('bargraph_perbarbyday', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'By Weekday'
      },
      xAxis: {
        categories: weekdays,
        title: {
          text: 'Day'
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Amount Spent'
        },
        labels: {
          overflow: 'justify'
        }
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        enabled: false
      },
      credits: {
        enabled: false
      },
      series: [{
        data: spents
      }]
    });
  }

  renderDrinkerBarByWeekChart(weeks: string[], spents: number[]) {
    Highcharts.chart('bargraph_perbarbyweek', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'By Week of Year'
      },
      xAxis: {
        categories: weeks,
        title: {
          text: 'Week of Year'
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Amount Spent'
        },
        labels: {
          overflow: 'justify'
        }
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        enabled: false
      },
      credits: {
        enabled: false
      },
      series: [{
        data: spents
      }]
    });
  }

  renderDrinkerBarByMonthChart(months: string[], spents: number[]) {
    Highcharts.chart('bargraph_perbarbymonth', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'By Month'
      },
      xAxis: {
        categories: months,
        title: {
          text: 'Month'
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Amount Spent'
        },
        labels: {
          overflow: 'justify'
        }
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        enabled: false
      },
      credits: {
        enabled: false
      },
      series: [{
        data: spents
      }]
    });
  }


}
