import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface Bar {
  Address: string;
  City: string;
  License: string;
  Name: string;
  Phone: string;
  State: string;
  }

export interface BarMenuItem {
  Bar: string;
  Beer: string;
  Manf: string;
  Price: number;
  }

@Injectable({
  providedIn: 'root'
})

export class BarsService {
  constructor(
    public http: HttpClient
  ) { }

  getBars() {
    return this.http.get<Bar[]>('/api/bar');
  }
  getBar(bar: string) {
    return this.http.get<Bar>('/api/bar/' + bar);
  }
  getMenu(bar: string) {
    return this.http.get<BarMenuItem[]>('/api/menu/' + bar);
  }

  getFrequentCounts() {
    return this.http.get<any[]>('/api/frequents-data');
  }

  getDaysOfWeek() {
    return this.http.get<string[]>('/api/days-of-week');
  }

  getBarTop10Beers(bar: string) {
    return this.http.get<any[]>('/api/bar-top10beers/' + bar);
  }

  getBarTop10BeersByDay(bar: string, dw: string) {
    return this.http.get<any[]>('/api/bar-top10beers/' + bar + '/' + dw);
  }

  getBarTop10Drinkers(bar: string) {
    return this.http.get<any[]>('/api/bar-top10drinkers/' + bar);
  }

  getBarSalesTimeDistribution(bar: string) {
    return this.http.get<any[]>('/api/bar-sales-timedistribution/' + bar);
  }

  getTop10BarsByBeerDay(beer: string, dw: string) {
    return this.http.get<any[]>('/api/top10BarsByBeerDay/' + beer + '/' + dw);
  }

  getBeers() {
    return this.http.get<any[]>('/api/beer');
  }

  getBeerNames() {
    return this.http.get<string[]>('/api/beer_names');
  }

  getFractionByWeekDay(bar: string) {
    return this.http.get<any[]>('/api/inventoryFracByWeekDay/' + bar);
  }
  getTop10BarsBySales(beer: string, inputDay: string) {
    return this.http.get<any[]>('/api/top10BarsBySales/' + beer + '/' + inputDay);
  }

}

