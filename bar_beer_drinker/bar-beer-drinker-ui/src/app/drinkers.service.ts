import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface Drinker {
  Name: string;
  Address: string;
  City: string;
  State: string;
  Phone: string;
  }

export interface DrinkerTransaction {
    Bar: string;
    Date: string;
    Time: string;
    TotalAmount: number;
    ItemsBought: string;
    }

@Injectable({
  providedIn: 'root'
})

export class DrinkersService {
  constructor(
    public http: HttpClient
  ) { }

  getDrinkers() {
    return this.http.get<Drinker[]>('/api/drinker');
  }
  getDrinker(drinker: string) {
    return this.http.get<Drinker>('/api/drinker/' + drinker);
  }
  getDrinkerTransactions(drinker: string) {
    return this.http.get<DrinkerTransaction[]>('/api/drinker-transactions/' + drinker);
  }
  getDrinkerTop10Beer(drinker: string) {
    return this.http.get<any[]>('/api/drinker-top10beer/' + drinker);
  }
  getDrinkerBars(drinker: string) {
    return this.http.get<string[]>('/api/drinker-bars/' + drinker);
  }
  getDrinkerBarByDay(drinker: string, bar: string) {
    return this.http.get<any[]>('/api/drinker-bar-byday/' + drinker + '/' + bar);
  }
  getDrinkerBarByWeek(drinker: string, bar: string) {
    return this.http.get<any[]>('/api/drinker-bar-byweek/' + drinker + '/' + bar);
  }
  getDrinkerBarByMonth(drinker: string, bar: string) {
    return this.http.get<any[]>('/api/drinker-bar-bymonth/' + drinker + '/' + bar);
  }
}
